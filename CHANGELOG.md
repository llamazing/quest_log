# Quest Log Menu Changelog

## v1.0.2
_04 Oct 2020_

### New
* Add unit tests to ensure features don't break in future versions (#89)
* Allow hiding of lines with via dynamic checkmark custom function (#91)
* Add functions to get current substitution values (#92)
* Add function to clear all objectives data (for unit tests) (#93)
* Allow access to basic global functions from objectives.dat scripts (#95)
* Allow localizations to reconfigure menu ui elements (#96)

### Improved
* Provide more details in error messages (#97)
* Display completed quests at top of list temporarily until viewed (#99)

### Bugfixes
* Fix for multi-line strings.dat entries (see [solarus-quest-editor#468](https://gitlab.com/solarus-games/solarus-quest-editor/-/issues/468))
* Fix bug with dynamic checkmark highlighting
* Fix bug with `;@` special character in desc text (#94)
* Extend tab text by 1 pixel to fix rendering in new version of Solarus (#100)

### Removed
* `new_objective:iter_checkmarks()` iterator was broken and useless, thus removed (#90)

## v1.0.1
_30 Dec 2019_

### New
* Manually trigger a quest update alert using game.objectives:force_update() (#77)
* Callback functions in objectives.dat have `args.n` parameter to get the number of savegame values in args (#78)
* Add new special character `#;;` (#81)
* Add new special characters `$!` & `$?` to affect mid-line visibility (#82)
* Allow both `$!` & `$?` special characters on the same line (#85)

### Improved
* Make "Main Quests" & "Side Quests" tabs wider to accomodate localization (in widescreen mode only) (#80)

### Bugfixes

* Dynamic checkmarks in description text were placed at the wrong position (#79)
* Fix for hard break `;;` special character so it works with debugger (#81)
* Ignore direction commands while pause menu closes (#83)
* Rapid toggling pause menu via func keys no longer breaks menu (#87)

## v1.0
_23 Jun 2019_

### Initial Release
