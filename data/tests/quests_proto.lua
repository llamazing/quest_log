--[[ quests_proto.lua
	version 0.1a
	10 Apr 2018
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/
	
	This script contains various functions to facilitate unit tests of the quest log menu.
	
	Usage:
	local quests_proto = require"tests/quest_proto"
	quest_proto:load_resources(path)
	--perform test(s)
]]

local quests_proto = {}

function quests_proto:load_data(path)
	local game = sol.main.get_game()
	local file
	
	if sol.main.get_type(path)=="map" then
		local map_id = path:get_id() --path is really a sol.map
		path = "maps/"..map_id:match"^(.-%/?)[^%/]+$"
	end
	
	--create items resources --TODO loading items dynamically doesn't work
	--[=[
	if sol.file.exists(path.."items.lua") then
		sol.file.mkdir"items" --create directory if does not exist
		
		--generate items resources in quest write directory
		local item_list = {} --keep track of item ids to prevent dupes
		local env = {}
		function env.item(properties)
			local item_data = {}
			
			local id = properties.id
			assert(id, "item resource does not contain key 'id'")
			assert(not item_list[id], "duplicate item id: "..id)
			assert(type(id)=="string", "item resource 'id' must be a string")
			
			local lua = properties.lua
			assert(type(lua)=="string" or not lua, "item resource 'lua' must be a string or nil")
			
			file = sol.file.open("items/"..id..".lua", "w")
			file:write(lua or "")
			file:close()
			
			if sol.main.resource_exists("item", id) then
				sol.main.remove_resource(resource_type, id) --overwrite existing if exists
			end
			sol.main.add_resource("item", id)
		end
		
		setmetatable(env, {__index = function() return function() end end}) --don't do anything if unhandled index encountered
		
		local chunk = sol.main.load_file(path.."items.lua")
		setfenv(chunk, env)
		chunk()
		
		file = sol.file.open("tests/quest_log_dialogs.dat", "rb")
		if file then
			dialogs = file:read"*all"
			file:close()
		end	
		file = sol.file.open("languages/unit_tests/text/dialogs.dat", "wb")
		file:write(dialogs)
		file:close()
	end
	]=]
	
	--## Create strings and dialogs resources
	
	--create directories if do not exist
	sol.file.mkdir"languages"
	sol.file.mkdir"languages/unit_tests"
	sol.file.mkdir"languages/unit_tests/text/"
	
	--copy quest log dialogs data to unit_tests language directory
	local dialogs = ""
	file = sol.file.open("tests/quest_log_dialogs.dat", "rb")
	if file then
		dialogs = file:read"*all"
		file:close()
	end	
	file = sol.file.open("languages/unit_tests/text/dialogs.dat", "wb")
	file:write(dialogs)
	file:close()
	
	--copy test dialogs data to unit_tests language directory
	local dialogs = ""
	file = sol.file.open(path.."dialogs.lua", "rb")
	if file then
		dialogs = file:read"*all"
		file:close()
	end	
	file = sol.file.open("languages/unit_tests/text/dialogs.dat", "ab")
	file:write(dialogs)
	file:close()
	
	--copy quest log strings data to unit_tests language directory
	local strings = ""
	file = sol.file.open("tests/quest_log_strings.dat", "rb")
	if file then
		strings = file:read"*all"
		file:close()
	end
	file = sol.file.open("languages/unit_tests/text/strings.dat", "wb")
	file:write(strings)
	file:close()
	
	--copy test strings data to unit_tests language directory
	local strings = ""
	file = sol.file.open(path.."strings.lua", "rb")
	if file then
		strings = file:read"*all"
		file:close()
	end
	file = sol.file.open("languages/unit_tests/text/strings.dat", "ab")
	file:write(strings)
	file:close()
	
	if not sol.main.resource_exists("language", "unit_tests") then
		sol.main.add_resource("language", "unit_tests")
	end
	sol.language.set_language"unit_tests"
	
	game.objectives:reset_data()
	game.objectives:load_data(path.."objectives.lua")
end

--// Compares the current desc text for the given quest id to the expected dialog id
	--quest_id (string) - dialogs.dat key used as the quest log quest identifier
	--expected_id (string) - dialogs.dat id corresponding to the expected desc text output
	--returns true - if the actual description text matches the expected dialogs.dat text
		--otherwise an error results, providing the first non-matching line of text
function quests_proto:assert_desc(quest_id, expected_raw)
	local game = sol.main.get_game()
	
	local actual_desc = game.objectives:get_objective(quest_id):get_description()
	--local actual_iter = actual_desc:gmatch"([^\n]+)\n"
	--local expected_raw = sol.language.get_dialog(expected_id).text
	local expected_desc = expected_raw:gsub("\t", ""):gsub("\r\n", "\n"):gsub("\r","\n")
	
	local line_index = 0
	for expected_line in expected_desc:gmatch"([^\n]*)\n" do
		line_index = line_index + 1
		local actual_line = (actual_desc[line_index] or {}).text or ""
		assert(expected_line==actual_line,
			string.format(
				"Desc line %d does not match; expected: %q, actual: %q",
				line_index, expected_line, actual_line
			)
		)
	end
	
	return true
end

function quests_proto:assert_grey(quest_id, expected_is_grey)
	local game = sol.main.get_game()
	
	local actual_desc = game.objectives:get_objective(quest_id):get_description()
	
	for i,expected in ipairs(expected_is_grey) do
		local actual = not not (actual_desc[i] or {}).is_grey
		assert(expected==actual,
			string.format(
				"Desc line %d greyed does not match; expected: %s, actual %s",
				i, expected, actual
			)
		)
	end
	
	return true
end

function quests_proto:assert_phase(quest_id, expected_phase)
	local game = sol.main.get_game()
	
	local actual_phase = game.objectives:get_objective(quest_id):get_current_phase()
	assert(expected_phase==actual_phase,
		string.format(
			"Phase does not match; expected: %q, actual: %q",
			expected_phase, actual_phase
		)
	)
	
	return true
end

function quests_proto:assert_status(quest_id, expected_status)
	local game = sol.main.get_game()
	
	local actual_status = game.objectives:get_objective(quest_id):get_status()
	assert(expected_status==actual_status,
		string.format(
			"Status does not match; expected: %q, actual: %q",
			expected_status, actual_status
		)
	)
end

function quests_proto:assert_location(quest_id, expected_location, phase)
	local game = sol.main.get_game()
	
	local actual_location = game.objectives:get_objective(quest_id):get_location_key(phase)
	assert(expected_location==actual_location,
		string.format(
			"Location key does not match; expected: %q, actual: %q",
			tostring(expected_location), tostring(actual_location)
		)
	)
	
	return true
end

function quests_proto:assert_npc(quest_id, expected_npc, phase)
	local game = sol.main.get_game()
	
	local actual_npc = game.objectives:get_objective(quest_id):get_npc_id(phase)
	assert(expected_npc==actual_npc,
		string.format(
			"NPC id does not match; expected: %q, actual: %q",
			expected_npc, actual_npc
		)
	)
	
	return true
end

function quests_proto:assert_s_keys(quest_id, expected_s_keys)
	local game = sol.main.get_game()
	
	local objective = game.objectives:get_objective(quest_id)
	for i=1,9 do
		local expected = expected_s_keys[i] or ""
		local actual = objective:get_s_key(i) or ""
		
		assert(expected==actual,
			string.format(
				"s_key #%d does not match; expected: %q, actual: %q",
				i, expected, actual
			)
		)
	end
	
	return true
end

function quests_proto:assert_v_values(quest_id, expected_v_values)
	local game = sol.main.get_game()
	
	local objective = game.objectives:get_objective(quest_id)
	for i=1,9 do
		local expected = expected_v_values[i] or ""
		local actual = objective:get_v_value(i) or ""
		
		assert(expected==actual,
			string.format(
				"v_value #%d does not match; expected: %q, actual: %q",
				i, expected, actual
			)
		)
	end
	
	return true
end

function quests_proto:assert_checkmarks(quest_id, expected_checkmarks)
	local game = sol.main.get_game()
	
	local objective = game.objectives:get_objective(quest_id)
	for i=1,9 do
		local expected = expected_checkmarks[i] or ""
		local actual = objective:get_checkmark(i) or ""
		
		assert(expected==actual,
			string.format(
				"dynamic checkmark #%d does not match; expected: %q, actual: %q",
				i, tostring(expected), tostring(actual)
			)
		)
	end
	
	return true
end

function quests_proto:assert_active_alt(alternate_key, expected_id)
	local game = sol.main.get_game()
	
	local objective = game.objectives:get_active_alternate(alternate_key)
	local actual_id = objective:get_dialog_id()
	assert(expected_id==actual_id,
		string.format(
			"active alternate does not match; expected: %q, actual: %q",
			expected_id, actual_id
		)
	)
	
	return true
end

function quests_proto:assert_active_npc(npc_id, is_expected)
	local game = sol.main.get_game()
	if is_expected==nil then is_expected = true end --default as true
	
	local is_actual = game.objectives:is_npc_active(npc_id)
	assert(is_expected==is_actual,
		string.format(
			"npc active status for %q does not match; expected: %s, actual: %s",
			tostring(npc_id), is_expected, is_actual
		)
	)
	
	return true
end

function quests_proto:assert_counts(objective_type, expected_completed, expected_total)
	local game = sol.main.get_game()
	
	local actual_completed, actual_total = game.objectives:get_counts(objective_type)
	assert(expected_completed==actual_completed,
		string.format(
			"Number of completed %s quests mis-match; expected: %s, actual %s",
			objective_type, tostring(expected_completed), tostring(actual_completed)
		)
	)
	
	if expected_total then
		assert(expected_total==actual_total,
			string.format(
				"Number of total %s quests mis-match; expected: %s, actual %s",
				objective_type, tostring(expected_total), tostring(actual_total)
			)
		)
	end
	
	return true
end

function quests_proto:assert_filler(quest_id, expected_index, ...)
	local game = sol.main.get_game()
	
	local actual_index = game.objectives:get_objective(quest_id):get_description(...).filler
	
	assert(expected_index==actual_index,
		string.format(
			"Filler line index does not match; expected: %s, actual: %s",
			expected_index, actual_index
		)
	)
	
	return true
end

return quests_proto

--[[ Copyright 2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
