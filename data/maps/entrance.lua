local map = ...
local game = map:get_game()


function map:on_opening_transition_finished()

end

local wizard_timer
function wizard_welcome:on_interaction()
	if not game:get_value"wizard_intro" then
		game:start_dialog("welcome_dialog", function()
			game:set_value("wizard_intro", true)
			
			if wizard_timer then wizard_timer:stop() end
			wizard_timer = sol.timer.start(map, 1600, function()
				self:set_enabled(false)
				sol.audio.play_sound"bomb"
			end)
		end)
	else game:start_dialog"welcome_dialog_2" end
end

map:register_event("on_started", function(self)
	if game:get_value"wizard_intro" then
		wizard_welcome:set_enabled(false)
	end
end)
