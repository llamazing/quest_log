local map = ...
local game = map:get_game()

local trade_quest = {
	witch = {
		[false] = { --quest not started
			dialog = "witch.trade_start",
			next_phase = 0,
			next_dialog = "witch.trade_start_2"
		},
		[0] = { --phase 0
			dialog = "witch.please_help",
		},
		{ --phase 1
			dialog = "witch.please_help",
		},
		{ --phase 2
			dialog = "witch.no_hat",
			encore = "witch.no_hat_2",
		},
		{ --phase 3
			dialog = "witch.need_bottle_for_spice",
			next_phase = 5,
		},
		{ --phase 4
			dialog = "witch.need_bottle_for_spice_2",
			next_phase = 5,
		},
		{ --phase 5
			dialog = "witch.find_bottle",
		},
		{ --phase 6
			dialog = "witch.cynthia_has_corkscrew",
			next_phase = 7,
		},
		{ --phase 7
			dialog = "witch.cynthia_has_corkscrew_2", --"witch.ask_about_corkscrew_2",
		},
		{ --phase 8
			dialog = "witch.ask_about_corkscrew",
			encore = "witch.ask_about_corkscrew_2",
		},
		{ --phase 9
			dialog = "witch.give_spice",
			next_phase = 12,
			remove_item = "empty_bottle",
			give_item = "spice",
		},
		{ --phase 10
			dialog = "witch.give_spice",
			next_phase = 11,
			remove_item = "empty_bottle",
			give_item = "spice",
		},
		{ --phase 11
			dialog = "witch.ask_about_mushroom",
		},
		{ --phase 12
			dialog = "witch.ask_about_mushroom",
		},
		{ --phase 13
			dialog = "witch.ask_about_mushroom",
		},
		{ --phase 14
			dialog = "witch.ask_about_mushroom",
		},
		{ --phase 15
			dialog = "witch.ask_about_mushroom",
		},
		{ --phase 16
			dialog = "witch.give_reward",
			remove_item = "mushroom",
			callback = function()
				local witch_sprite = witch:get_sprite()
				local initial_direction = witch_sprite:get_direction()
				
				hero:freeze()
				sol.timer.start(map, 650, function()
				witch_sprite:set_direction(0) --look right
				mushroom:set_enabled(true)
				sol.timer.start(map, 350, function()
				sol.audio.play_sound"throw"
				local movt = sol.movement.create"pixel"
				movt:set_trajectory{
					{1,0},
					{1,0},
					{1,1},
					{1,1},
					{1,2},
					{1,3},
					{1,4},
					{1,5},
				}
				movt:set_delay(100)
				movt:start(mushroom, function()
				mushroom:set_enabled(false)
				sol.audio.play_sound"splash"
				sol.timer.start(map, 500, function()
				witch_sprite:set_direction(initial_direction) --look at player again
				hero:unfreeze()
				game:start_dialog("witch.give_reward_2", function()
				hero:start_treasure("medal_ingenuity", 1, "item_medal_ingenuity", function()
				game:start_dialog("witch.give_reward_3", function()
				game:set_value("trade_quest", 17) --advance to phase 17 (quest done)
				end) end) end) end) end) end) end)
			end
		},
		{ --phase = 17 (quest complete)
			dialog = "witch.thanks_again",
		},
	},
	theodore = {
		[false] = { --quest not started
			dialog = "theodore.greetings",
		},
		[0] = { --phase 0
			dialog = "theodore.need_hat",
			next_phase = 2,
		},
		{ --phase 1
			dialog = "theodore.need_hat_2",
			next_phase = 2,
		},
		{ --phase 2
			dialog = "theodore.ask_about_hat",
		},
		{ --phase 3
			dialog = "theodore.witch_has_spice",
			next_phase = 4,
		},
		{ --phase 4
			dialog = "theodore.witch_has_spice_2",
		},
		{ --phase 5
			dialog = "theodore.need_corkscrew_for_bottle",
			next_phase = 6,
		},
		{ --phase 6
			dialog = "theodore.ask_about_corkscrew",
		},
		{ --phase 7
			dialog = "theodore.ask_about_corkscrew",
		},
		{ --phase 8
			dialog = "theodore.give_bottle",
			next_phase = 9,
			remove_item = "corkscrew",
			callback = function()
				wine_bottle:set_enabled(false)
				game:set_value("drank_bottle", true)
				game:start_dialog("theodore.give_bottle_2", function()
					--give empty bottle after dialog is finished
					local save_val = game:get_item"empty_bottle":get_savegame_variable()
					hero:start_treasure("empty_bottle", 1, save_val)
				end)
			end,
		},
		{ --phase 9
			dialog = "theodore.remembered_corkscrew",
			next_phase = 10,
			give_item = "corkscrew",
		},
		{ --phase 10
			dialog = "theodore.reminder_bottle",
		},
		{ --phase 11
			dialog = "theodore.reminder_bottle_2",
		},
		{ --phase 12
			dialog = "theodore.reminder_bottle_2",
		},
		{ --phase 13
			dialog = "theodore.give_corkscrew",
			next_phase = 14,
			give_item = "corkscrew",
		},
		{ --phase 14
			dialog = "theodore.still_need_mushroom",
		},
		{ --phase 15
			dialog = "theodore.give_mushroom",
			next_phase = 16,
			next_dialog = "theodore.give_mushroom_2",
			remove_item = "hat",
			callback = function()
				hat:set_enabled(true)
				game:start_dialog("theodore.give_mushroom_2", function()
					hero:start_treasure("mushroom", 1, "item_mushroom")
				end)
			end
		},
		{ --phase 16
			dialog = "theodore.thanks_again",
		},
		{ --phase 17 (quest done)
			dialog = "theodore.thanks_again",
		},
	},
	cynthia = {
		[false] = { --quest not started
			dialog = "cynthia.greetings_2",
			encore = "cynthia.drinking_tea",
		},
		[0] = { --phase 0
			dialog = "cynthia.theodore_has_mushroom",
			next_phase = 1,
		},
		{ --phase 1
			dialog = "cynthia.theodore_has_mushroom_2",
		},
		{ --phase 2
			dialog = "cynthia.need_spice_for_hat",
			next_phase = 3,
		},
		{ --phase 3
			dialog = "cynthia.need_spice_for_hat_2",
		},
		{ --phase 4
			dialog = "cynthia.need_spice_for_hat_2",
		},
		{ --phase 5
			dialog = "cynthia.no_bottle",
			encore = "cynthia.no_bottle_2",
		},
		{ --phase 6
			dialog = "cynthia.give_corkscrew",
			next_phase = 8,
			give_item = "corkscrew",
		},
		{ --phase 7
			dialog = "cynthia.give_corkscrew_2",
			next_phase = 8,
			give_item = "corkscrew",
		},
		{ --phase 8
			dialog = "cynthia.corkscrew_reminder",
		},
		{ --phase 9
			dialog = "cynthia.ask_about_spice",
		},
		{ --phase 10
			dialog = "cynthia.ask_about_spice",
		},
		{ --phase 11
			dialog = "cynthia.take_spice_and_corkscrew",
			next_phase = 15,
			remove_item = {"spice", "corkscrew"},
			give_item = "hat",
		},
		{ --phase 12
			dialog = "cynthia.need_corkscrew_for_hat",
			next_phase = 13,
			remove_item = "spice",
		},
		{ --phase 13
			dialog = "cynthia.need_corkscrew_for_hat_2",
		},
		{ --phase 14
			dialog = "cynthia.give_hat",
			next_phase = 15,
			remove_item = "corkscrew",
			give_item = "hat",
		},
		{ --phase 15
			dialog = "cynthia.thanks_again",
		},
		{ --phase 16
			dialog = "cynthia.thanks_again",
		},
		{ --phase 17 (quest done)
			dialog = "cynthia.thanks_again",
		},
	},
}

local next_dialog

local function get_interaction(npc_name)
	local current_phase = game:get_value"trade_quest" or false
	local data = trade_quest[npc_name][current_phase]
	
	if not data.is_done then
		game:start_dialog(data.dialog, function()
			data.is_done = true
			--take item
			if data.remove_item then --can be string (one item) or table (list of items)
				local items = data.remove_item
				if type(items)=="string" then items = {data.remove_item} end
				for _,item in ipairs(items) do
					local save_val = game:get_item(item):get_savegame_variable()
					game:set_value(save_val, nil)
				end
			end
			--give item
			if data.give_item then
				local save_val = game:get_item(data.give_item):get_savegame_variable()
				hero:start_treasure(data.give_item, 1, save_val)
			end
			if data.callback then data.callback() end
			if data.next_phase then game:set_value("trade_quest", data.next_phase) end
			next_dialog = data.next_dialog
		end)
	else game:start_dialog(data.encore or data.dialog) end
end

function witch:on_interaction()
	get_interaction(self:get_name())
end

function theodore:on_interaction()
	get_interaction(self:get_name())
end

function cynthia:on_interaction()
	--display initial greeting dialog only if it is the first time talking to Cynthia
	local is_greeting = game:get_value"cynthia_intro"
	if not is_greeting then
		game:set_value("cynthia_intro", true)
		game:start_dialog("cynthia.greetings", function()
			get_interaction(self:get_name())
		end)
	else get_interaction(self:get_name()) end
end

function map:resume_dialog()
	if next_dialog then game:start_dialog(next_dialog) end
	next_dialog = nil
end

map:register_event("on_started", function(self)
	if game:get_value"drank_bottle" then
		wine_bottle:set_enabled(false)
	end
	
	mushroom:set_enabled(false)
	
	local current_phase = game:get_value"trade_quest" or 0
	if current_phase <= 13 then hat:set_enabled(false) end
	
	local hat_sprite = hat:get_sprite()
	local theo_sprite = theodore:get_sprite()
	
	--synchronize hat with theodore
	function theo_sprite:on_direction_changed()
		hat_sprite:set_frame(self:get_frame())
		self:synchronize(hat_sprite)
	end
end)
