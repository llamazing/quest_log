local map = ...
local game = map:get_game()

local GEMSTONES = {
	emerald = 5,
	topaz = 3,
	ruby = 2,
}

local function get_gems_remaining()
	local emerald_count = game:get_value"item_count_emerald" or 0 --0 to 5
	local topaz_count = game:get_value"item_count_topaz" or 0 --0 to 3
	local ruby_count = game:get_value"item_count_ruby" or 0 --0 to 2
	
	local remaining = 10 - math.min(emerald_count, 5) - math.min(topaz_count, 3)
	 - math.min(ruby_count, 2)
	
	return remaining
end

-- Event called at initialization time, as soon as this map becomes is loaded.
map:register_event("on_started", function(self)
	--hide gemstones equal to the number that the player has already found
	for gemstone,room_count in pairs(GEMSTONES) do
		local found_count = game:get_value("item_count_"..gemstone) or 0
		for i = 1,math.min(found_count, room_count) do
			local entity = map:get_entity(gemstone.."_"..i)
			entity:set_enabled(false)
		end
	end
	
	if not game:get_value"gems_intro" then
		wizard_gems:set_position(264, 125)
		wizard_gems:get_sprite():set_direction(2)
		wizard_gems:set_visible(false)
	elseif game:get_value"wizard_gems_exit" then
		wizard_gems:set_enabled(false)
	end
	
	exit_up:set_enabled(false)
	exit_down:set_enabled(false)
end)

map:register_event("on_opening_transition_finished", function(_)
	--cutscene
	if not game:get_value"gems_intro" then
		--start cutscene: wizard enters map and begins dialog
		hero:freeze()
		game:start_dialog("gems_intro", function()
		sol.timer.start(map, 350, function()
		wizard_gems:set_visible(true)
		local movt = sol.movement.create"path"
		movt:set_path{4,4,4,4,4,4,4,4,4,4,4,4,4,6,6,6}
		movt:set_speed(65)
		movt:start(wizard_gems, function()
		wizard_gems:get_sprite():set_direction(2)
		game:start_dialog("gems_intro_1", function()
		game:set_value("gems_intro", true)
		hero:unfreeze()
		end) end) end) end)
	end
end)

function wizard_gems:on_interaction()
	local remaining = get_gems_remaining()
	
	if remaining==10 then
		game:start_dialog("gems_intro_2", remaining)
	elseif remaining > 0 then
		game:start_dialog("gems_intro_3", remaining)
	else game:start_dialog"gems_done" end
end

local function sensor_on_exit(sensor)
	local remaining = get_gems_remaining()
	if remaining <= 0 and not game:get_value"wizard_gems_exit" then
		hero:freeze()
		hero:set_visible(false)
		sol.timer.start(map, 1600, function()
			wizard_gems:set_enabled(false)
			sol.audio.play_sound"bomb"
			game:set_value("wizard_gems_exit", true)
			sol.timer.start(map, 900, function()
				hero:unfreeze()
				exit_up:set_enabled(true)
				exit_down:set_enabled(true)
			end)
		end)
	else
		exit_up:set_enabled(true)
		exit_down:set_enabled(true)
	end
end

function sensor_exit_up:on_activated() sensor_on_exit(self) end
function sensor_exit_down:on_activated() sensor_on_exit(self) end
function map:on_finished() game:get_hero():set_visible(true) end