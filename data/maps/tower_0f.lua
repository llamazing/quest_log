local map = ...
local game = map:get_game()

function switch_east:on_activated()
	game:set_value("east_switch", true)
end

map:register_event("on_started", function(self)
	if game:get_value"east_switch" then
		switch_east:set_activated(true)
	end
end)