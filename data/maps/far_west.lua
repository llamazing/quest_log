-- Lua script of map far_west.
-- This script is executed every time the hero enters this map.

local wavy_motion = require"scripts/fx/wavy_motion"

local map = ...
local game = map:get_game()

map:register_event("on_started", function(self)
	if game:get_value"west_switch" then
		switch_west:set_activated(true)
	end
	
	local alt_intro = game:get_value"alternates_intro"
	
	--hide orbs if in any phase besides phase 0
	if alt_intro ~= 0 then
		orb_2:set_enabled(false)
		orb_3:set_enabled(false)
		orb_5:set_enabled(false)
	end
	
	--save game when entering room if quest has not been started yet
	if not alt_intro then
		game:set_starting_location("far_west", "start")
		game:save()
	end
	
	if game.is_loading_save then
		local camera = map:get_camera()
		wavy_motion:start_effect(camera:get_surface(), map, "FADE_IN")
		game.is_loading_save = nil
	end
end)

function switch_west:on_activated()
	game:set_value("west_switch", true)
end

--called whenever player hits the armor some number of times
function armor:on_last_hit(hit_count)
	local alt_intro = game:get_value"alternates_intro" or 0
	if alt_intro > 0 and alt_intro <= 5 then
		local save_var = "hit_"..hit_count
		if not game:get_value(save_var) then
			game:set_value(save_var, true)
		end
		
		--determine current phase of whichever alternate quest is active
		local objective = game.objectives:get_active_alternate"choose_your_quest"
		local phase = objective:get_current_phase()
		
		if phase < 2 then --quest is not complete, hit wrong number of times
			game:start_dialog("alternates_wrong", game:get_value"alternates_intro") --alternates_intro now stores number of hits needed
		else game:set_value("alternates_intro", 6) end --quest is now done
	end
end

local ALT_NAMES = { --quest id corresponding to each orb item
	orb_2 = "quest.double_trouble",
	orb_3 = "quest.treasured_third",
	orb_5 = "quest.fifth_gift",
}
function map:on_orb_obtained(item)
	--get rid of orbs the player didn't choose
	if orb_2 then orb_2:remove() end
	if orb_3 then orb_3:remove() end
	if orb_5 then orb_5:remove() end
	
	local item_name = item:get_name()
	local hits_needed = tonumber(item_name:match"orb_(%d)")
	game:set_value("alternates_intro", hits_needed) --can check this save val to know that orb obtained if > 0
	
	--change the alternate quest to match the orb chosen
	local quest_id = ALT_NAMES[item_name]
	game.objectives:set_alternate("choose_your_quest", quest_id)
end

local function reset_cutscene()
	hero:freeze()
	sol.timer.start(map, 400, function()
	wizard_alt:get_sprite():set_animation"spell"
	local camera = map:get_camera()
	sol.timer.start(map, 0, function() sol.audio.play_sound"time_warp"; return 640 end)
	wavy_motion:start_effect(camera:get_surface(), map, "FADE_OUT")
	sol.timer.start(map, 2600, function()
	hero:unfreeze()
	sol.main.reset()
	end) end)
end

function wizard_alt:on_interaction()
	local alt_intro = game:get_value"alternates_intro"
	if not alt_intro then
		game:start_dialog("alternates_intro", function()
			game:set_value("alternates_intro", 0)
			
			sol.timer.start(map, 400, function()
				sol.audio.play_sound"teleporter"
				orb_2:set_enabled(true)
				orb_3:set_enabled(true)
				orb_5:set_enabled(true)
			end)
		end)
	elseif alt_intro == 0 then
		game:start_dialog"alternates_intro_2"
	elseif alt_intro < 5 then
		game:start_dialog("alternates_hit", alt_intro)  --alternates_intro now stores number of hits needed
	else
		game:start_dialog("alternates_ask_redo", function(answer)
			if answer == 1 then
				game:start_dialog("alternates_redo", function()
					reset_cutscene()
				end)
			else game:start_dialog"alternates_just_ask" end
		end)
	end
end

function exit_sensor:on_activated()
	local alt_intro = game:get_value"alternates_intro" or 0
	if alt_intro > 5 and not game:get_value"loaded_save" then
		game:start_dialog("alternates_hold_it", function()
			reset_cutscene()
		end)
	end
end