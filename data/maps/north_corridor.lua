-- Lua script of map north_corridor.
-- This script is executed every time the hero enters this map.

local map = ...
local game = map:get_game()
local hero = game:get_hero()

local enemy_count = 0 --ensure no more than 3 enemies spawned at one time
local chest_timer
local spawn_timer
local spawn_check

local function spawn_timer_cb()
	is_success = spawn_check()
	if not is_success then spawn_timer = nil end
	
	return is_success
end

local function on_enemy_dead(self)
	enemy_count = enemy_count - 1
	
	local kill_count = (game:get_value"slimes_killed" or 0) + 1
	game:set_value("slimes_killed", kill_count)
	
	if not spawn_timer then spawn_timer = sol.timer.start(map, 3000, spawn_timer_cb) end --start respawn timer if not already active
	
	--make chest appear, use timer to retry every 250ms if player is standing on chest location
	if kill_count >=6 and not chest_medal:is_enabled() and not chest_timer then
		chest_timer = sol.timer.start(map, 750, function()
			if not chest_medal:overlaps(hero) then
				chest_medal:set_enabled(true)
				sol.audio.play_sound"chest_appears"
			else
				if chest_timer then chest_timer:stop() end
				chest_timer = sol.timer.start(map, 250, function()
					if not chest_medal:overlaps(hero) then
						chest_medal:set_enabled(true)
						sol.audio.play_sound"chest_appears"
						return false
					else return true end
				end)
			end
		end)
	end
end

spawn_check = function()
	if enemy_count < 3 then
		local dist_left = hero:get_distance(spawn_left)
		local dist_right = hero:get_distance(spawn_right)
		
		--use the spawn point farthest from player to spawn new enemy
		local spawn = dist_right > dist_left and spawn_right or spawn_left
		local spawn_x, spawn_y, spawn_layer = spawn:get_position() 
		
		local new_enemy = map:create_enemy{
			name = "slime",
			layer = spawn_layer,
			x = spawn_x,
			y = spawn_y,
			direction = 3, --south
			breed = "slime_green",
		}
		
		new_enemy.on_dead = on_enemy_dead
		enemy_count = enemy_count + 1
		
		sol.audio.play_sound"lift"
		
		return true
	else return false end
end

map:register_event("on_started", function(self)
	--hide wizard, slime & magic_deck if player has already seen intro cutscene
	if game:get_value"slimes_intro" then
		wizard_monsters:set_enabled(false)
		first_slime:set_enabled(false)
		magic_deck:set_enabled(false)
		
		spawn_timer = sol.timer.start(map, 3000, spawn_timer_cb)
	end
	
	--activate switch if had been activated previously
	if game:get_value"north_switch" then
		switch_north:set_activated(true)
	end
	
	--hide chest if player hasn't killed enough slimes
	if (game:get_value"slimes_killed" or 0)<6 then
		chest_medal:set_enabled(false)
	end
end)

map:register_event("on_opening_transition_finished", function(_)
	--cutscene
	if not game:get_value"slimes_intro" then
		--start cutscene: wizard kills slime, starts dialog, then walks away
		hero:freeze()
		wizard_monsters:get_sprite():set_animation"spell"
		sol.timer.start(map, 850, function()
		local movt = sol.movement.create"target"
		movt:set_target(first_slime, 0, 5)
		movt:set_speed(135)
		movt:start(magic_deck, function()
		movt:stop()
		first_slime:set_life(0)
		magic_deck:remove()
		sol.timer.start(map, 350, function()
		wizard_monsters:get_sprite():set_animation"stopped"
		wizard_monsters:get_sprite():set_direction(3)
		game:start_dialog("slimes_intro", function()
		game:set_value("slimes_intro", true)
		movt = sol.movement.create"path"
		movt:set_path{4,4,4,6,6,6,6,6,6,6,6,6,6,6,6}
		movt:set_speed(65)
		movt:start(wizard_monsters, function()
		wizard_monsters:set_enabled(false)
		hero:unfreeze()
		spawn_timer = sol.timer.start(map, 3000, spawn_timer_cb)
		end) end) end) end) end)
	end
end)

function wizard_monsters:on_interaction()
	game:start_dialog("slimes_intro", function()
		game:set_value("slimes_intro", true)
	end)
end

function switch_north:on_activated()
	game:set_value("north_switch", true)
end


