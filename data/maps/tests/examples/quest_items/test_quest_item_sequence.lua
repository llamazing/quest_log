--test of the quest item sequence example
--see: https://gitlab.com/llamazing/quest_log/-/wikis/API/dialogs.dat#example-using-quest-items-sequence 

local map = ...
local game = map:get_game()
local hero = game:get_hero()

function map:on_started()
	local quests_proto = require"tests/quests_proto"
	quests_proto:load_data(map)
	
	local quest_id = "example.item_sequence"
	game:set_value("quest_item_sequence", 0) --start quest
	
	quests_proto:assert_desc(quest_id, [[
Grab the key from Oswald

You do not have any items
]])
	
	game:set_value("item_orb_2", 1)
	
	quests_proto:assert_desc(quest_id, [[
Grab the key from Oswald

Use the key to unlock the
gate behind the windmill

QUEST ITEM: 
Orb 2
]])
	
	game:set_value("is_gate_unlocked", true)
	
	quests_proto:assert_desc(quest_id, [[
Grab the key from Oswald

Use the key to unlock the
gate behind the windmill

Grab a shovel from Wendy

QUEST ITEM: 
Orb 2
]])
	
	game:set_value("item_corkscrew", 1)
	
	quests_proto:assert_desc(quest_id, [[
Grab the key from Oswald

Use the key to unlock the
gate behind the windmill

Grab a shovel from Wendy

Dig up the treasure

QUEST ITEM: 
Corkscrew
]])
	
	game:set_value("item_topaz", 1)
	
	quests_proto:assert_desc(quest_id, [[
Grab the key from Oswald

Use the key to unlock the
gate behind the windmill

Grab a shovel from Wendy

Dig up the treasure

Show the treasure to Tony

QUEST ITEM: 
Topaz
]])
	
	sol.main.exit()
end

--[[
@	Grab the key from Oswald

@	Use the key to unlock the
	gate behind the windmill

@	Grab a shovel from Wendy

@	Dig up the treasure

@	Show the treasure to Tony
#
#%~	You do not have any items
#%=	QUEST ITEM: $i0
#%=	$ITEM_NAME
]]
