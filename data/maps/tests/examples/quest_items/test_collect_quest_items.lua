--test of the collect all quest items example
--see: https://gitlab.com/llamazing/quest_log/-/wikis/API/dialogs.dat#example-using-quest-items-collect-them-all

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "example.collect_items"
game:set_value("collect_items", 0) --start quest

quests_proto:assert_desc(quest_id, [[
Collect 3 artifacts

Artifacts Collected:
None
]])

game:set_value("item_orb_3", 1)

quests_proto:assert_desc(quest_id, [[
Collect 3 artifacts

Artifacts Collected:
            

Fancy Vase
]])

game:set_value("item_orb_2", 1)

quests_proto:assert_desc(quest_id, [[
Collect 3 artifacts

Artifacts Collected:
            

Sun Stone
Fancy Vase
]])

game:set_value("item_orb_5", 1)

quests_proto:assert_desc(quest_id, [[
Collect 3 artifacts

Artifacts Collected:
            

Sun Stone
Fancy Vase
Jade Statue
]])

game:set_value("item_orb_3", nil)

quests_proto:assert_desc(quest_id, [[
Collect 3 artifacts

Artifacts Collected:
            

Sun Stone
Jade Statue
]])

sol.main.exit()

--[[
	Collect 3 artifacts

	Artifacts Collected:
%~	None
%=	$i1      $i2      $i3
%=
%1	Sun Stone
%2	Fancy Vase
%3	Jade Statue
]]
