--maps/tests/examples/quest_items/test_quest_item_sequence
dialog{
	id = "example.item_sequence",
	title = "Item Sequence",
	text = [[
@	Grab the key from Oswald

@	Use the key to unlock the
	gate behind the windmill

@	Grab a shovel from Wendy

@	Dig up the treasure

@	Show the treasure to Tony
#
#%~	You do not have any items
#%=	QUEST ITEM: $i0
#%=	$ITEM_NAME
]]}

--maps/tests/examples/quest_items/test_collect_quest_items
dialog{
	id = "example.collect_items",
	title = "Collect Items",
	text = [[
	Collect 3 artifacts

	Artifacts Collected:
%~	None
%=	$i1      $i2      $i3
%=
%1	Sun Stone
%2	Fancy Vase
%3	Jade Statue
]]}
