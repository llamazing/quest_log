text{ key = "item.emerald.1", value = "Emerald" }
text{ key = "item.topaz.1", value = "Topaz" }
text{ key = "item.ruby.1", value = "Ruby" }
text{ key = "item.orb_2.1", value = "Orb 2" }
text{ key = "item.orb_3.1", value = "Orb 3" }
text{ key = "item.orb_5.1", value = "Orb 5" }
text{ key = "item.corkscrew.1", value = "Corkscrew" }
text{ key = "item.empty_bottle.1", value = "Empty Bottle" }
text{ key = "item.spice.1", value = "Spice" }
text{ key = "item.hat.1", value = "Hat" }
text{ key = "item.mushroom.1", value = "Mushroom" }
text{ key = "item.medal_bravery.1", value = "Medal of Bravery" }
text{ key = "item.medal_vigor.1", value = "Medal of Vigor" }
text{ key = "item.medal_ingenuity.1", value = "Medal of Ingenuity" }