--maps/tests/examples/quest_items/test_quest_item_sequence
main{
	dialog_id = "example.item_sequence",
	calc_phase = {
		"quest_item_sequence",	--false: quest not started, 0: quest started, incomplete, 1: quest done
		"item_orb_2", --true if player has gate key
		"is_gate_unlocked", --true if gate has been unlocked
		"item_corkscrew", --true if player has shovel
		"item_topaz", --true if player dug up treasure
		callback = function(args)
			local quest_sequence = args[1]
			local has_orb = (args[2] or 0) >= 1
			local is_gate_unlocked = args[3] or false
			local has_corkscrew = (args[4] or 0) >= 1
			local has_treasure = (args[5] or 0) >= 1
			
			if quest_sequence==0 then --quest is in progress
				if not has_treasure then --player hasn't found treasure yet
					if not has_corkscrew then --player doesn't have shovel yet
						if not is_gate_unlocked then --gate has not been unlocked yet
							if not has_orb then --player doesn't have key yet
								return 0 --player needs to grab key
							else return 1 end --player needs to unlock gate
						else return 2 end --player needs to grab shovel
					else return 3 end --player needs to dig up treasure
				else return 4 end --player needs to show treasure to Tony
			elseif quest_sequence==1 then --player already showed treasure to Tony
				return 5 --quest is complete
			else return false end --quest not started
		end
	},
	items = {
		"orb_2.1", --instead of gate key
		"corkscrew.1", --instead of shovel
		"topaz.1", --instead of treasure
	},
}

--maps/tests/examples/quest_items/test_collect_quest_items
main{
	dialog_id = "example.collect_items",
	calc_phase = "collect_items",
	items = {
		"orb_2.1",
		"orb_3.1",
		"orb_5.1",
	},
}
