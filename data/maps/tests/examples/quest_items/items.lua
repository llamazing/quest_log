--maps/tests/examples/quest_items/test_quest_item_sequence
item{id="gate_key", lua=[[
local item = ...
local game = item:get_game()

function item:on_started()
	self:set_savegame_variable"inventory_gate_key"
	self:set_assignable(false)
	self:set_brandish_when_picked(false)
end
]]}
item{id="shovel", lua=[[
local item = ...
local game = item:get_game()

function item:on_started()
	self:set_savegame_variable"inventory_shovel"
	self:set_assignable(false)
	self:set_brandish_when_picked(false)
end
]]}
item{id="treasure", lua=[[
local item = ...
local game = item:get_game()

function item:on_started()
	self:set_savegame_variable"inventory_treasure"
	self:set_assignable(false)
	self:set_brandish_when_picked(false)
end
]]}
