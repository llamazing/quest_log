--Text showing how many monsters remaining to be killed is removed once task is complete (alternate method)
--see: https://gitlab.com/llamazing/quest_log/-/wikis/Examples/hiding-variable-substitution-when-phase-is-complete#method-2-using-the-split-line-special-characters

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "example.kill_slimes_alt"
game:set_value("slimes_intro", 0) --start quest

quests_proto:assert_desc(quest_id, [[
Monsters are swarming the
north corridor. Clean up
this menace.

Kill 6 slimes (6 remaining) 
]])

game:set_value("slimes_killed", 5) --kill all but 1 of required number of slimes to kill

quests_proto:assert_desc(quest_id, [[
Monsters are swarming the
north corridor. Clean up
this menace.

Kill 6 slimes (1 remaining) 
]])

game:set_value("slimes_killed", 6) --kill all but 1 of required number of slimes to kill

quests_proto:assert_desc(quest_id, [[
Monsters are swarming the
north corridor. Clean up
this menace.

Kill 6 slimes 

Claim the treasure
]])

sol.main.exit()
--[[
Monsters are swarming the
north corridor. Clean up
this menace.

@Kill 6 slimes $!($v1 remaining) --//remaining number of slimes hidden once task complete

@Claim the treasure
]]
