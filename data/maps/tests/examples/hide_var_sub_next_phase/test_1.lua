--Text showing how many monsters remaining to be killed is removed once task is complete
--see: https://gitlab.com/llamazing/quest_log/-/wikis/Examples/hiding-variable-substitution-when-phase-is-complete#method-1-variable-substitution-that-depends-on-current-phase

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "example.kill_slimes"
game:set_value("slimes_intro", 0) --start quest

quests_proto:assert_desc(quest_id, [[
Monsters are swarming the
north corridor. Clean up
this menace.

Kill 6 slimes (6 remaining) 
]])

game:set_value("slimes_killed", 5) --kill all but 1 of required number of slimes to kill

quests_proto:assert_desc(quest_id, [[
Monsters are swarming the
north corridor. Clean up
this menace.

Kill 6 slimes (1 remaining) 
]])

game:set_value("slimes_killed", 6) --kill all but 1 of required number of slimes to kill

quests_proto:assert_desc(quest_id, [[
Monsters are swarming the
north corridor. Clean up
this menace.

Kill 6 slimes  

Claim the treasure
]])

sol.main.exit()
--[[
Monsters are swarming the
north corridor. Clean up
this menace.

@Kill 6 slimes $s1 --// $s1 is replaced with strings.dat entry "quest.kill_slimes.remaining",
--// which indicates how many more monsters the player must kill as $v1.

@Claim the treasure
]]
