--maps/tests/examples/hide_var_sub_next_phase/test_1
side{
	dialog_id = "example.kill_slimes",
	location = "location.north_corridor",
	npc = {
		start = "wizard_monsters", --talking to wizard starts this quest
	},
	calc_phase = {
		"slimes_intro", --true if player watched into cutscene to obtain quest
		"slimes_killed", --(number, non-negative integer) number of slimes the player has killed, nil if none
		"possession_flask", --true if player obtained the reward at the end of the quest, which completes it
		callback = function(args)
			local is_intro = args[1] or false
			local kill_count = args[2] or 0
			local is_flask = args[3] or false
			
			if is_intro then
				if kill_count>=6 then
					if is_flask then
						return 2
					else return 1 end
				else return 0 end
			else return false end
		end
	},
	replace_s = {
		callback = function(args)
			if (args.phase or 0) < 1 then --player has not killed all the slimes yet
				return {"quest.kill_slimes.remaining"} --substitutes for "($v1 remaining)" where $v1 is the number of slimes the player still needs to kill
			else return {false} end --substitute for empty string, no longer need to show how many are remaining
		end
	},
	replace_v = { --always substitute for $v1 regardless of whether $v1 exists in the dialog or not
		"slimes_killed", --(number, non-negative integer) number of slimes the player has killed, nil if none
		callback = function(args)
			local kill_count = args[1] or 0
			local remaining_count = 6 - kill_count
			return {remaining_count}
		end
	},
}

--maps/tests/examples/hide_var_sub_next_phase/test_2
side{
	dialog_id = "example.kill_slimes_alt",
	location = "location.north_corridor",
	npc = {
		start = "wizard_monsters", --talking to wizard starts this quest
	},
	calc_phase = {
		"slimes_intro", --true if player watched into cutscene to obtain quest
		"slimes_killed", --(number, non-negative integer) number of slimes the player has killed, nil if none
		"possession_flask", --true if player obtained the reward at the end of the quest, which completes it
		callback = function(args)
			local is_intro = args[1] or false
			local kill_count = args[2] or 0
			local is_flask = args[3] or false
			
			if is_intro then
				if kill_count>=6 then
					if is_flask then
						return 2
					else return 1 end
				else return 0 end
			else return false end
		end
	},
	replace_v = {
		"slimes_killed", --(number, non-negative integer) number of slimes the player has killed, nil if none
		callback = function(args)
			local kill_count = args[1] or 0
			local remaining_count = 6 - kill_count
			return {remaining_count}
		end
	},
}
