--maps/tests/examples/hide_var_sub_next_phase/test_1
dialog{
	id = "example.kill_slimes",
	-------max line width-------
	title = "Defeat Monsters",
	text = [[
	Monsters are swarming the
	north corridor. Clean up
	this menace.
	
@	Kill 6 slimes $s1 --// $s1 is replaced with strings.dat entry "quest.kill_slimes.remaining",
	                  --// which indicates how many more monsters the player must kill as $v1.

@	Claim the treasure
]]}

--maps/tests/examples/hide_var_sub_next_phase/test_2
dialog{
	id = "example.kill_slimes_alt",
	-------max line width-------
	title = "Defeat Monsters",
	text = [[
	Monsters are swarming the
	north corridor. Clean up
	this menace.
	
@	Kill 6 slimes $!($v1 remaining) --//remaining number of slimes hidden once task complete

@	Claim the treasure
]]}
