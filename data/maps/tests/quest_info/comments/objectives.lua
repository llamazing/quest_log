--maps/tests/quest_info/comments/test_comments
main{
	dialog_id = "test.comments",
	calc_phase = "comments",
	replace_s = {
		"unused",
		callback = function(args)
			return {(args.phase or 0)>0 and "comments.sub1" or "comments.sub2"}
		end
	},
}
