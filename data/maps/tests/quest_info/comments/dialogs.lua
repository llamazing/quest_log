--maps/tests/quest_info/comments/test_comments
dialog{
	id = "test.comments",
	title = "Comments Test",
	text = [[
@phase 1 --//This is a comment
       --//This is is skipped entirely
!--//not skipped phase 1 only
sub $s1 obscured
start $!temporary --//comment$?mystery
begin $?hidden --//comment$!fleeting
@phase 2
#end
]]}
