--tests comments in quest log desc text

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "test.comments"
game:set_value("comments", 0) --start quest

quests_proto:assert_desc(quest_id, [[
phase 1 

sub no comment obscured
start temporary 
begin 
end
]])

game:set_value("comments", 1)

quests_proto:assert_desc(quest_id, [[
phase 1 
sub text 
start 
begin hidden 
phase 2
end
]])

sol.main.exit()

--dialog reference
--[[
@phase 1 --//This is a comment
       --//This is is skipped entirely
!--//not skipped during phase 1 only
sub $s obscured
start $!temporary --//comment$?mystery
begin $?hidden --//comment$!fleeting
@phase 2
#end
]]
