--maps/tests/quest_info/location/test_one_location
dialog{
	id = "test.one_location",
	title = "One Location Test",
	text = [[
@phase1
@phase2
]]}

--maps/tests/quest_info/location/test_multi_locations
dialog{
	id = "test.multi_locations",
	title = "Multiple Locations Test",
	text = [[
@phase1
@phase2
@phase3
@phase4
]]}