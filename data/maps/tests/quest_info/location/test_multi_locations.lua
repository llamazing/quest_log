--tests multiple locations given as a table

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "test.multi_locations"
game:set_value("multi_locations", 0) --start quest

quests_proto:assert_location(quest_id, "location.forest_grove")

game:set_value("multi_locations", 1)

quests_proto:assert_location(quest_id, "location.underwater_palace")

game:set_value("multi_locations", 2)

quests_proto:assert_location(quest_id, false)

game:set_value("multi_locations", 3)

quests_proto:assert_location(quest_id, "location.mountain_summit")

game:set_value("multi_locations", 4) --complete quest

quests_proto:assert_location(quest_id, "location.mountain_summit") --no change

sol.main.exit()
