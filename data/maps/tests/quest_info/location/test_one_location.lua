--tests a single location given as a string

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "test.one_location"
game:set_value("one_location", 0) --start quest

quests_proto:assert_location(quest_id, "location.castle1")

game:set_value("one_location", 1)

quests_proto:assert_location(quest_id, "location.castle1")

game:set_value("one_location", 2) --complete quest

quests_proto:assert_location(quest_id, "location.castle1")

sol.main.exit()
