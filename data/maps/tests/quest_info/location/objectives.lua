--throws error if actual location key (string or false) does not matches expected
local function assert_loc(expected, actual)
	assert(actual==expected, string.format(
		"Unexpected custom function location value; expected: %q, actual: %q",
		tostring(expected), tostring(actual)
	))
end

--common function to use for test_one_location
local function loc_test_cb(args)
	assert_loc(args.location_key, "location.castle1")
	return {}
end

--maps/tests/quest_info/location/test_one_location
main{
	dialog_id = "test.one_location",
	location = "location.castle1",
	calc_phase =  {
		"one_location",
		callback = function(args)
			assert_loc(args.location_key, "location.castle1")
			return args[1]
		end
	},
	replace_s = {
		callback = loc_test_cb,
	},
	replace_v = {
		callback = loc_test_cb,
	},
	checkmarks = {
		callback = loc_test_cb,
	},
}

--common function to use for test_multi_locations
local function multi_loc_test_cb(args)
	local expected_locs = {
		[0] = "location.forest_grove",
		"location.underwater_palace",
		false,
		"location.mountain_summit",
		"location.mountain_summit", --after quest is complete
	}
	
	assert_loc(expected_locs[args.phase] or false, args.location_key) --note args.phase is nil when calc_phase called first time (hence `or false`)
	return {}
end

--maps/tests/quest_info/location/test_multi_locations
main{
	dialog_id = "test.multi_locations",
	location = {
		"location.forest_grove",
		"location.underwater_palace",
		false,
		"location.mountain_summit",
	},
	calc_phase =  {
		"multi_locations",
		callback = function(args)
			multi_loc_test_cb(args)
			return args[1]
		end
	},
	replace_s = {
		callback = multi_loc_test_cb,
	},
	replace_v = {
		callback = multi_loc_test_cb,
	},
	checkmarks = {
		callback = multi_loc_test_cb,
	},
}
