--maps/tests/quest_info/location/test_one_location
text{ key = "location.castle1", value = "Bridgewater Castle" }

--maps/tests/quest_info/location/test_multi_locations
text{ key = "location.forest_grove", value = "Heart of the Forest" }
text{ key = "location.underwater_palace", value = "Underwater Palace" }
text{ key = "location.mountain_summit", value = "Top of the World" }
