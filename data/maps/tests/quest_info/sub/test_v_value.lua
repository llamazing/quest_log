--tests various value substitutions in quest description text (without custom function)

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "test.sub_v_value"
game:set_value("v_value_phase", 0) --start quest

--initially all substitutions are empty string (phase 0)
quests_proto:assert_desc(quest_id, [[
hi    end
more
always visible 
]])

--substitute values
game:set_value("val_sub_1", true)
game:set_value("val_sub_2", "3.2") --need to convert numbers with decimal to string first to preserve decimal
game:set_value("val_sub_3", "hope")
game:set_value("val_sub_4", 7)
game:set_value("val_sub_5", -10)
quests_proto:assert_desc(quest_id, [[
hi 3.2 hope true end
more
3.2
always visible -10
]])

--substitute different values
game:set_value("val_sub_1", "")
game:set_value("val_sub_2", "0.9")
game:set_value("val_sub_3", "joy")
game:set_value("val_sub_4", 5)
game:set_value("val_sub_5", -1)
quests_proto:assert_desc(quest_id, [[
hi 0.9 joy  end
more
0.9
always visible -1
]])

--next phase
game:set_value("v_value_phase", 1)
quests_proto:assert_desc(quest_id, [[
0.9
5 phase 2
always visible -1
]])

--complete quest
game:set_value("v_value_phase", 2)
quests_proto:assert_desc(quest_id, [[
0.9
5 phase 2
always visible -1
]])

sol.main.exit()

--dialog reference
--[[
@!	hi $v2 $v3 $v1 end
 !	more
	$v2
@	$v4 phase 2
##	always visible $v5
]]
