--tests various value substitutions in quest description text using a custom function

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "test.sub_custom_func"
game:set_value("custom_func_phase", 0) --start quest

--initially all substitutions are empty string (phase 0)
quests_proto:assert_desc(quest_id, [[




]])

--show dynamic checkmarks values
game:set_value("check_1", false)
game:set_value("check_2", true)
game:set_value("check_3", false)
quests_proto:assert_desc(quest_id, [[
 some value 0
 second another line
 2.1
 no  bullet
]])

--substitute values
game:set_value("val_sub_1", true)
game:set_value("val_sub_2", 1)
game:set_value("val_sub_3", "b")
game:set_value("val_sub_4", true)
quests_proto:assert_desc(quest_id, [[
 some value 1
 first another line
 7.4
 no true bullet
]])

--more change-up
game:set_value("val_sub_2", 2)
game:set_value("val_sub_1", false)
game:set_value("check_2", nil)
game:set_value("check_3", nil)
quests_proto:assert_desc(quest_id, [[
 some value 2
 second another line


]])

sol.main.exit()

--dialog reference
--[[
$@1some value $v2
$1 $v1 another line
$@2$v3
$3 no $v4 bullet
]]
