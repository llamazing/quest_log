--tests various string substitutions in quest description text

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "test.sub_s_key" --convenience
game:set_value("s_key_phase", 0) --start quest

--initially all substitutions are empty string (phase 0)
quests_proto:assert_desc(quest_id, [[
more  tests
is  visible
]])

--set substitutions
game:set_value("str_sub_1", "s_key_1") --"first"
game:set_value("str_sub_2", "s_key_2") --"two words"
game:set_value("str_sub_3", "s_key_3") --"extra"
quests_proto:assert_desc(quest_id, [[
more first tests
is two words visible
]])

--change substitutions
game:set_value("str_sub_1", "s_key_2") --"two words"
game:set_value("str_sub_2", "s_key_1") --"first"
quests_proto:assert_desc(quest_id, [[
more two words tests
is first visible
]])

--change phase (phase 1)
game:set_value("s_key_phase", 1)
quests_proto:assert_desc(quest_id, [[
more two words tests
is not visible
 sub within extra sub
]])

--do val sub within str sub
game:set_value("str_sub_3", "s_v_key") --"value $v1"
game:set_value("str_val_sub", 5)
quests_proto:assert_desc(quest_id, [[
more two words tests
is not visible
 sub within value 5 sub
]])

--sub with comment
game:set_value("str_sub_1", "s_key_comment") --"comment--//hidden"
quests_proto:assert_desc(quest_id, [[
more comment
is not visible
 sub within value 5 sub
]])

sol.main.exit()

--dialog reference
--[[
@more $s1 tests
is $?not visible$!$s2 visible
@ sub within $s3 sub
]]