dialog{
	id = "test.sub_v_value",
	title = "Value Substitution",
	text = [[
@!	hi $v2 $v3 $v1 end	--//multiple substitutions on same line
 !	more	--//ignore substitution $v1 in comment
	$v2	--//repeat of substitution above
@	$v4 phase 2	--//not visible until phase 2
##	always visible $v5
]]}

dialog{
	id = "test.sub_custom_func",
	title = "Custom Function Substitution",
	text = [[
$@1some value $v2
$1 $v1 another line
$@2$v3
$3 no $v4 bullet
]]}

dialog{
	id = "test.sub_s_key",
	title = "String Substitution",
	text = [[
@more $s1 tests
is $?not visible$!$s2 visible
@ sub within $s3 sub
]]}
