main{
	dialog_id = "test.sub_v_value",
	calc_phase = "v_value_phase",
	replace_v = {
		"val_sub_1",
		"val_sub_2",
		"val_sub_3",
		"val_sub_4",
		"val_sub_5",
	},
}

main{
	dialog_id = "test.sub_custom_func",
	calc_phase = "custom_func_phase",
	replace_v = {
		"val_sub_1", --nil/true
		"val_sub_2", --nil/1
		"val_sub_3", --"a"/"b"
		"val_sub_4", --false/true
		callback = function(args)
			local v1 = args[1]
			local v2 = args[2] or 0
			local v3 = args[3] or "a"
			local v4 = args[4] or false
			
			return {
				v1 and "first" or "second",
				v2,
				v3=="b" and 7.4 or 2.1,
				v4,
			}
		end
	},
	checkmarks = {
		"check_1",
		"check_2",
		"check_3",
		callback = function(args)
			return {args[1], args[2], args[3]}
		end
	},
}

side{
	dialog_id = "test.sub_s_key",
	calc_phase = "s_key_phase",
	replace_v = {
		"str_val_sub",
	},
	replace_s = {
		"str_sub_1",
		"str_sub_2",
		"str_sub_3",
		callback = function(args)
			return {
				args[1] or false,
				args[2] or false,
				args[3] or false,
			}
		end
	},
}
