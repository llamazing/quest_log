--tests hard break pattern `;;` in quest log description text

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "test.hard_break"
game:set_value("hard_break", 0) --start quest

quests_proto:assert_desc(quest_id, [[
phase 1
visible except phase 2
hide after phase 3
]])

game:set_value("hard_break", 1)

quests_proto:assert_desc(quest_id, [[
phase 1
phase 2
next line
]])

game:set_value("hard_break", 2)

quests_proto:assert_desc(quest_id, [[
phase 1
phase 2
next line
visible except phase 2

phase 3
hide after phase 3
]])

game:set_value("hard_break", 3)

quests_proto:assert_desc(quest_id, [[
phase 1
phase 2
next line
visible except phase 2

phase 3
phase 4

visible phases 4 & 5
]])

game:set_value("hard_break", 4)

quests_proto:assert_desc(quest_id, [[
phase 1
phase 2
next line
visible except phase 2

phase 3
phase 4
phase 5

visible phases 4 & 5
]])

game:set_value("hard_break", 5) --quest complete

quests_proto:assert_desc(quest_id, [[
phase 1
phase 2
next line
visible except phase 2

phase 3
phase 4
phase 5

visible phases 4 & 5
]])

sol.main.exit()

--dialog reference
--[[
@phase 1
@phase 2
next line
;; --//hard break only during phase 2
#visible except phase 2

@phase 3
#!hide after phase 3
#;; --//hard break phases 1-3 only
@phase 4
@phase 5
#
#visible phases 4 & 5
]]