--maps/tests/quest_info/phase_breaks/test_phase_breaks
dialog{
	id = "test.phase_breaks",
	title = "Phase Breaks Test",
	text = [[
Begin

@phase 1 task
continue next line

start phase 2

@phase 2 task
!
!still phase 2

start phase 3
@phase 3 task

still phase 3

;@phase 4 task
continue next line
@phase 5 task
continue next line
--// comment line
still phase 5

@phase 6 task
continue next line
?
?still phase 6
#
#Always visible

more text
@phase 7
]]}

--maps/tests/quest_info/phase_breaks/test_hard_break
dialog{
	id = "test.hard_break",
	title = "Hard Break Test",
	text = [[
@phase 1
@phase 2
next line
;; --//hard break only during phase 2
#visible except phase 2

@phase 3
#!hide after phase 3
#;; --//hard break phases 1-3 only
@phase 4
@phase 5
#
#visible phases 4 & 5
]]}

--maps/tests/quest_info/phase_breaks/test_hidden_phases
dialog{
	id = "test.hidden_phases",
	title = "Hidden Phases Test",
	text = [[
@phase 1

also phase 1
;!?
@!temporary

also phase 2
;!?
@?mystery

also phase 3
;@!?always hide
also phase 4
#always visible
]]}