--tests various phase break patterns in quest log description text

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "test.phase_breaks"
game:set_value("phase_breaks", 0) --start quest

quests_proto:assert_desc(quest_id, [[
Begin

phase 1 task
continue next line

Always visible
]])

game:set_value("phase_breaks", 1)

quests_proto:assert_desc(quest_id, [[
Begin

phase 1 task
continue next line

start phase 2

phase 2 task

still phase 2

Always visible
]])

game:set_value("phase_breaks", 2)

quests_proto:assert_desc(quest_id, [[
Begin

phase 1 task
continue next line

start phase 2

phase 2 task

start phase 3
phase 3 task

still phase 3


Always visible
]])

game:set_value("phase_breaks", 3)

quests_proto:assert_desc(quest_id, [[
Begin

phase 1 task
continue next line

start phase 2

phase 2 task

start phase 3
phase 3 task

still phase 3

phase 4 task
continue next line

Always visible
]])

game:set_value("phase_breaks", 4)

quests_proto:assert_desc(quest_id, [[
Begin

phase 1 task
continue next line

start phase 2

phase 2 task

start phase 3
phase 3 task

still phase 3

phase 4 task
continue next line
phase 5 task
continue next line
still phase 5

Always visible
]])

game:set_value("phase_breaks", 5)

quests_proto:assert_desc(quest_id, [[
Begin

phase 1 task
continue next line

start phase 2

phase 2 task

start phase 3
phase 3 task

still phase 3

phase 4 task
continue next line
phase 5 task
continue next line
still phase 5

phase 6 task
continue next line

Always visible
]])

game:set_value("phase_breaks", 6)

quests_proto:assert_desc(quest_id, [[
Begin

phase 1 task
continue next line

start phase 2

phase 2 task

start phase 3
phase 3 task

still phase 3

phase 4 task
continue next line
phase 5 task
continue next line
still phase 5

phase 6 task
continue next line

still phase 6

Always visible
]])

game:set_value("phase_breaks", 7)

quests_proto:assert_desc(quest_id, [[
Begin

phase 1 task
continue next line

start phase 2

phase 2 task

start phase 3
phase 3 task

still phase 3

phase 4 task
continue next line
phase 5 task
continue next line
still phase 5

phase 6 task
continue next line

still phase 6

Always visible

more text
phase 7
]])

sol.main.exit()

--dialog reference
--[[
@phase 1
@phase 2
next line
;; --//hard break only during phase 2
#visible except phase 2

@phase 3
#;; --//hard break phases 1-3
@phase 4
@phase 5
#
#Always visible
]]
