--test map: tests/quest_info/phase_breaks/test_phase_breaks
main{
	dialog_id = "test.phase_breaks",
	calc_phase = "phase_breaks",
}

--test map: tests/quest_info/phase_breaks/test_hard_break
main{
	dialog_id = "test.hard_break",
	calc_phase = "hard_break",
}

--test map: tests/quest_info/phase_breaks/test_hidden_phases
main{
	dialog_id = "test.hidden_phases",
	calc_phase = "hidden_phases",
}
