--tests hard break pattern `;;` in quest log description text

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "test.hidden_phases"
game:set_value("hidden_phases", 0) --start quest

quests_proto:assert_desc(quest_id, [[
phase 1

also phase 1
always visible
]])

game:set_value("hidden_phases", 1)

quests_proto:assert_desc(quest_id, [[
phase 1

also phase 1
temporary

also phase 2
always visible
]])

game:set_value("hidden_phases", 2)

quests_proto:assert_desc(quest_id, [[
phase 1

also phase 1

also phase 2

also phase 3
always visible
]])

game:set_value("hidden_phases", 3)

quests_proto:assert_desc(quest_id, [[
phase 1

also phase 1

also phase 2
mystery

also phase 3
also phase 4
always visible
]])

sol.main.exit()

--dialog reference
--[[
@phase 1

also phase 1
;@!temporary

also phase 2
;@?mystery

also phase 3
;@!?always hide
also phase 4
#always visible
]]