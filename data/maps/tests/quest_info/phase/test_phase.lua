--tests changing the quest phase using a custom function

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "test.phase"

game:set_value("phase", 0) --start quest
quests_proto:assert_phase(quest_id, 0)

game:set_value("phase", 1)
quests_proto:assert_phase(quest_id, 1)

game:set_value("phase", 2)
quests_proto:assert_phase(quest_id, 2)

game:set_value("phase", 1)
quests_proto:assert_phase(quest_id, 1)

game:set_value("phase", 3)
quests_proto:assert_phase(quest_id, 3)

game:set_value("phase", 2)
quests_proto:assert_phase(quest_id, 3) --quest previously marked complete, cannot be uncompleted

sol.main.exit()
