--tests changing the quest phase using a custom function

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "test.phase_custom_function"

game:set_value("quest_started", true) --start quest
quests_proto:assert_phase(quest_id, 0)

game:set_value("phase_val_1", true)
quests_proto:assert_phase(quest_id, 1)

game:set_value("phase_val_2a", true)
quests_proto:assert_phase(quest_id, 1)

game:set_value("phase_val_1", false)
quests_proto:assert_phase(quest_id, 0)

game:set_value("phase_val_2b", true)
quests_proto:assert_phase(quest_id, 2)

game:set_value("phase_val_3", 2)
quests_proto:assert_phase(quest_id, 2)

game:set_value("phase_val_1", true)
quests_proto:assert_phase(quest_id, 2)

game:set_value("phase_val_3", 5)
quests_proto:assert_phase(quest_id, 3)

game:set_value("phase_val_3", 4)
quests_proto:assert_phase(quest_id, 3) --quest previously marked complete, cannot be uncompleted

sol.main.exit()
