--maps/tests/quest_info/phase/test_phase
main{
	dialog_id = "test.phase",
	calc_phase = "phase",
}

--maps/tests/quest_info/phase/test_custom_function
main{
	dialog_id = "test.phase_custom_function",
	calc_phase = {
		"quest_started", --nil/true
		"phase_val_1", --nil/true
		"phase_val_2a", --nil/true
		"phase_val_2b", --nil/true
		"phase_val_3", --nil/number (0-5)
		callback = function(args)
			local is_started = args[1]
			local v1 = args[2]
			local v2a = args[3]
			local v2b = args[4]
			local v3_count = args[5] or 0
			
			if v3_count >= 5 then return 3 --quest complete
			elseif v2a and v2b then return 2
			elseif v1 then return 1
			elseif is_started then return 0
			else return nil end
		end
	},
}
