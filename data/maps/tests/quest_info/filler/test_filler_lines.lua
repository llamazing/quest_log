--tests filler lines in quest log description text
--assumes the description contains up to 14 lines of text

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "test.filler_lines"
game:set_value("filler", 0) --start quest

quests_proto:assert_filler(quest_id, 2)
quests_proto:assert_desc(quest_id, [[
phase 1
put on bottom
]])

game:set_value("filler", 1)

quests_proto:assert_filler(quest_id, 5)
quests_proto:assert_desc(quest_id, [[
phase 1
start phase 2
phase 2
more phase 2
put on bottom
]])

game:set_value("filler", 2)

quests_proto:assert_filler(quest_id, 6)
quests_proto:assert_desc(quest_id, [[
phase 1
start phase 2
phase 2
more phase 2
phase 3
put on bottom
]])

game:set_value("filler", 3)

quests_proto:assert_filler(quest_id, nil)
quests_proto:assert_desc(quest_id, [[
phase 1
start phase 2
phase 2
more phase 2
phase 3
phase 4
more phase 4
new bottom
]])

game:set_value("filler", 4)

quests_proto:assert_filler(quest_id, 7)
quests_proto:assert_desc(quest_id, [[
phase 1
start phase 2
phase 2
more phase 2
phase 3
phase 4
more phase 4
phase 5
new bottom
]])

sol.main.exit()

--dialog reference
--[[
@phase 1
::!
;start phase 2
@phase 2
more phase 2
@phase 3
#::!
#!put on bottom
#;;
@phase 4
::?
more phase 4
@phase 5
#new bottom
]]
