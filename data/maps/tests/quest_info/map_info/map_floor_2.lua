local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"

function map:on_opening_transition_finished()
	local quest_id = "test.map_floors"
	quests_proto:assert_desc(quest_id, [[
floor 2
]])
	
	sol.main.exit()
end

--dialogs.dat reference
--[[
@!floor 0
 !extra
@!floor 1
@floor 2
]]
