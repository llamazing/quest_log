local map = ...
local game = map:get_game()
local hero = game:get_hero()

local quests_proto = require"tests/quests_proto"

function map:on_opening_transition_finished()
	local quest_id = "test.map_ids"
	
	quests_proto:assert_checkmarks(quest_id, {false, true, true})
	
	hero:teleport("tests/quest_info/map_info/map_id_first")
end

--dialogs.dat reference
--[[
Visit the following maps:
  $@1 first map
  $@2 second map
  $@3 third map
]]
