--tests the current map id with dynamic checkmarks
--teleports hero to map_id_second then map_id_third then map_id_first

local map = ...
local game = map:get_game()
local hero = game:get_hero()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

function map:on_opening_transition_finished()
	local quest_id = "test.map_ids"
	game:set_value("map_ids", true) --start quest
	
	quests_proto:assert_checkmarks(quest_id, {false, false, false})
	
	hero:teleport("tests/quest_info/map_info/map_id_second")
end

--dialogs.dat reference
--[[
Visit the following maps:
  $@1 first map
  $@2 second map
  $@3 third map
]]
