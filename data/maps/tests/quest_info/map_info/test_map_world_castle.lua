--tests the current map world as a value substitution
--teleports hero to world_dungeon then world_none

local map = ...
local game = map:get_game()
local hero = game:get_hero()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

function map:on_opening_transition_finished()
	local quest_id = "test.map_worlds"
	game:set_value("map_worlds", 0) --start quest
	
	quests_proto:assert_desc(quest_id, [[
The current world is: Castle
]])
	
	hero:teleport("tests/quest_info/map_info/map_world_dungeon")
end

--dialogs.dat reference
--[[
The current world is: $v1
]]
