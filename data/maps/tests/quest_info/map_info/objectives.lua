--test map: tests/quest_info/map_info/test_map_floor_1
main{
	dialog_id = "test.map_floors",
	calc_phase = {
	"started_floor_quest",
	"$MAP_FLOOR",
	callback = function(args)
		return args[1] and args[2]
	end,
	},
}

--test map: tests/quest_info/map_info/test_map_floor_1
main{
	dialog_id = "test.map_worlds",
	calc_phase = "map_worlds",
	replace_v = {
		"$MAP_WORLD",
		callback = function(args)
			return {args[1] or "not defined"}
		end,
	},
}

--maps/tests/quest_info/map_info/test_map_id
local visited = {}
main{
	dialog_id = "test.map_ids",
	calc_phase = {
		"map_ids",
		"$MAP_ID",
		callback = function(args)
			visited[ args[2] ] = true
			return args[1] and (visited['tests/quest_info/map_info/map_id_first']
			 and visited['tests/quest_info/map_info/map_id_second']
			 and visited['tests/quest_info/map_info/map_id_third'] and 1 or 0)
		end
	},	
	checkmarks = {
		"$MAP_ID",
		callback = function(args)
			return {
				visited['tests/quest_info/map_info/map_id_first'] or false,
				visited['tests/quest_info/map_info/map_id_second'] or false,
				visited['tests/quest_info/map_info/map_id_third'] or false,
			}
		end,
	},
}
