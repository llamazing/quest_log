local map = ...
local game = map:get_game()
local hero = game:get_hero()

local quests_proto = require"tests/quests_proto"

function map:on_opening_transition_finished()
	local quest_id = "test.map_floors"
	quests_proto:assert_desc(quest_id, [[
floor 0
extra
]])
	
	hero:teleport("tests/quest_info/map_info/map_floor_2")
end

--dialogs.dat reference
--[[
@!floor 0
 !extra
@!floor 1
@floor 2
]]
