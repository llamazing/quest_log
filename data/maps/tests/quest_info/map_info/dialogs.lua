--maps/tests/quest_info/map_info/test_map_floor_1
dialog{
	id = "test.map_floors",
	title = "Map Floor Test",
	text = [[
@!floor 0
 !extra
@!floor 1
@floor 2
]]}

--maps/tests/quest_info/map_info/test_map_world_castle
dialog{
	id = "test.map_worlds",
	title = "Map Worlds Test",
	text = [[
The current world is: $v1
]]}

--maps/tests/quest_info/map_info/test_map_id
dialog{
	id = "test.map_ids",
	title = "Map IDs Test",
	text = [[
Visit the following maps:
  $@1 first map
  $@2 second map
  $@3 third map
]]}
