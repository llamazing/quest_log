local map = ...
local game = map:get_game()
local hero = game:get_hero()

local quests_proto = require"tests/quests_proto"

function map:on_opening_transition_finished()
	local quest_id = "test.map_worlds"
	quests_proto:assert_desc(quest_id, [[
The current world is: Dungeon
]])
	
	hero:teleport("tests/quest_info/map_info/map_world_none")
end

--dialogs.dat reference
--[[
The current world is: $v1
]]
