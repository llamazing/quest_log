local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"

function map:on_opening_transition_finished()
	local quest_id = "test.map_ids"
	
	quests_proto:assert_checkmarks(quest_id, {true, true, true})
	quests_proto:assert_phase(quest_id, 1)
	
	sol.main.exit()
end

--dialogs.dat reference
--[[
Visit the following maps:
  $@1 first map
  $@2 second map
  $@3 third map
]]
