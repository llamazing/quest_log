--tests the current quest phase being set by the current map floor
--teleports hero to floor_0 then floor_2

local map = ...
local game = map:get_game()
local hero = game:get_hero()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

function map:on_opening_transition_finished()
	local quest_id = "test.map_floors"
	game:set_value("started_floor_quest", 0) --start quest
	
	quests_proto:assert_desc(quest_id, [[
floor 1
]])
	
	hero:teleport("tests/quest_info/map_info/map_floor_0")
end

--dialogs.dat reference
--[[
@!floor 0
 !extra
@!floor 1
@floor 2
]]
