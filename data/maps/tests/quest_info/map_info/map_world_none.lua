local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"

function map:on_opening_transition_finished()
	local quest_id = "test.map_worlds"
	quests_proto:assert_desc(quest_id, [[
The current world is: not defined
]])
	
	sol.main.exit()
end

--dialogs.dat reference
--[[
The current world is: $v1
]]
