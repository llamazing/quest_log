--tests the active npc each phase

local map = ...
local game = map:get_game()
local hero = game:get_hero()

local quests_proto = require"tests/quests_proto"

local phase = game:get_value"active_npc"
if not phase then quests_proto:load_data(map) end --first time loading map only

function map:on_opening_transition_finished()
	if not phase then --first time entered map
		quests_proto:assert_active_npc("bob", true)
		quests_proto:assert_active_npc("jane", false)
		quests_proto:assert_active_npc("dummy", false)
		
		game:set_value("active_npc", 0) --start quest
		
		quests_proto:assert_active_npc("bob", false)
		quests_proto:assert_active_npc("jane", true)
		quests_proto:assert_active_npc("dummy", false)
		
		hero:teleport("tests/quest_info/npc/active_npc_2")
	elseif phase == 0 then
		quests_proto:assert_active_npc("bob", false)
		quests_proto:assert_active_npc("jane", true)
		quests_proto:assert_active_npc("dummy", false)
		
		game:set_value("active_npc", 1)
		
		quests_proto:assert_active_npc("bob", false)
		quests_proto:assert_active_npc("jane", false)
		quests_proto:assert_active_npc("dummy", false)
		
		hero:teleport("tests/quest_info/npc/active_npc_2")
	elseif phase == 1 then
		quests_proto:assert_active_npc("bob", false)
		quests_proto:assert_active_npc("jane", false)
		quests_proto:assert_active_npc("dummy", false)
		
		game:set_value("active_npc", 2) --completes quest
		
		quests_proto:assert_active_npc("bob", false)
		quests_proto:assert_active_npc("jane", false)
		quests_proto:assert_active_npc("dummy", false)
		
		hero:teleport("tests/quest_info/npc/active_npc_2")
	else
		quests_proto:assert_active_npc("bob", false)
		quests_proto:assert_active_npc("jane", false)
		quests_proto:assert_active_npc("dummy", false)
		
		sol.main.exit()
	end
end
