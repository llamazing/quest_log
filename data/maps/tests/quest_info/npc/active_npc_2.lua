--tests the active npc each phase

local map = ...
local game = map:get_game()
local hero = game:get_hero()

local quests_proto = require"tests/quests_proto"

function map:on_opening_transition_finished()
	local phase = game:get_value"active_npc" or 0
	
	if phase <= 0 then
		quests_proto:assert_active_npc("bob", false)
		quests_proto:assert_active_npc("jane", true)
		quests_proto:assert_active_npc("dummy", false)
		
		hero:teleport("tests/quest_info/npc/test_active_npc")
	elseif phase == 1 then
		quests_proto:assert_active_npc("bob", false)
		quests_proto:assert_active_npc("jane", false)
		quests_proto:assert_active_npc("dummy", false)
		
		hero:teleport("tests/quest_info/npc/test_active_npc")
	else
		quests_proto:assert_active_npc("bob", false)
		quests_proto:assert_active_npc("jane", false)
		quests_proto:assert_active_npc("dummy", false)
		
		hero:teleport("tests/quest_info/npc/test_active_npc")
	end
end
