--maps/tests/quest_info/quest_counts/test_quest_counts
main{
	dialog_id = "test.counts_1",
	calc_phase = "counts_1",
}
main{
	dialog_id = "test.counts_2a",
	calc_phase = "counts_2",
	alternate_key = "alt_2",
}
main{
	dialog_id = "test.counts_2b",
	calc_phase = {
		"is_alt_2",
		"counts_2",
		callback = function(args)
			return args[1] and args[2]
		end,
	},
	alternate_key = "alt_2",
}
main{
	dialog_id = "test.counts_3",
	calc_phase = "counts_3",
}
side{
	dialog_id = "test.counts_4",
	calc_phase = "counts_4",
}
side{
	dialog_id = "test.counts_5a",
	calc_phase = "counts_5",
	alternate_key = "alt_5",
}
side{
	dialog_id = "test.counts_5b",
	calc_phase = {
		"is_alt_5",
		"counts_5",
		callback = function(args)
			return args[1] and args[2]
		end,
	},
	alternate_key = "alt_5",
}
