--tests the quest counter updating when quests are completed

local map = ...
local game = map:get_game()

local status_counts = {}
function game.objectives:on_quest_updated(status, dialog_id)
	local count = status_counts[status] or 0
	status_counts[status] = count + 1
end

local function assert_status_count(status, expected_count)
	local actual_count = status_counts[status] or 0
	assert(expected_count==actual_count,
		string.format(
			"status %q count mis-match; expected: %s, actual %s",
			tostring(status), tostring(expected_count), tostring(actual_count)
		)
	)
	
	return true
end

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

quests_proto:assert_counts("main", 0, 3)
quests_proto:assert_counts("side", 0, 2)
assert_status_count("main_started", 0)
assert_status_count("main_completed", 0)
assert_status_count("side_started", 0)
assert_status_count("side_completed", 0)
assert_status_count("alternate_swap", 0)
assert_status_count("main_all_completed", 0)
assert_status_count("side_all_completed", 0)

game:set_value("counts_1", 0) --start quest
assert_status_count("main_started", 1)
assert_status_count("side_started", 0)
game:set_value("counts_1", 1) --complete quest

quests_proto:assert_counts("main", 1, 3)
quests_proto:assert_counts("side", 0, 2)
assert_status_count("main_completed", 1)
assert_status_count("side_completed", 0)
assert_status_count("alternate_swap", 0)
assert_status_count("main_all_completed", 0)
assert_status_count("side_all_completed", 0)

game:set_value("counts_4", 0) --start quest
assert_status_count("main_started", 1)
assert_status_count("side_started", 1)
game:set_value("counts_4", 1) --complete quest

quests_proto:assert_counts("main", 1, 3)
quests_proto:assert_counts("side", 1, 2)
assert_status_count("main_completed", 1)
assert_status_count("side_completed", 1)
assert_status_count("alternate_swap", 0)
assert_status_count("main_all_completed", 0)
assert_status_count("side_all_completed", 0)

game:set_value("counts_2", 0) --start quest
assert_status_count("main_started", 2)
assert_status_count("side_started", 1)
game:set_value("is_alt_2", true) --enable alt quest
game.objectives:set_alternate("alt_2", "test.counts_2b") --switch to alt quest
assert_status_count("alternate_swap", 1)
game:set_value("counts_2", 1) --complete quest

quests_proto:assert_counts("main", 2, 3)
quests_proto:assert_counts("side", 1, 2)
assert_status_count("main_completed", 2)
assert_status_count("side_completed", 1)
assert_status_count("main_all_completed", 0)
assert_status_count("side_all_completed", 0)

game:set_value("counts_3", 0) --start quest
assert_status_count("main_started", 3)
assert_status_count("side_started", 1)
game:set_value("counts_3", 1) --complete quest

quests_proto:assert_counts("main", 3, 3)
quests_proto:assert_counts("side", 1, 2)
assert_status_count("main_completed", 2) --"main_all_completed" status instead of "main_completed" so count remains at 2
assert_status_count("side_completed", 1)
assert_status_count("alternate_swap", 1)
assert_status_count("main_all_completed", 1)
assert_status_count("side_all_completed", 0)

game:set_value("counts_5", 0) --start quest
assert_status_count("main_started", 3)
assert_status_count("side_started", 2)
assert_status_count("alternate_swap", 1) --didn't do an alternate swap this time
game:set_value("counts_5", 1) --complete quest

quests_proto:assert_counts("main", 3, 3)
quests_proto:assert_counts("side", 2, 2)
assert_status_count("main_completed", 2)
assert_status_count("side_completed", 1)
assert_status_count("main_all_completed", 1)
assert_status_count("side_all_completed", 1)

sol.main.exit()
