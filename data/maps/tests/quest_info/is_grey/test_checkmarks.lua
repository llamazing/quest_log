--tests whether description dynamic checkmark lines grey out or not

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "test.grey_checkmarks"
game:set_value("is_grey", 0) --start quest

quests_proto:assert_grey(quest_id, {false, false, false, false, false, false})
quests_proto:assert_desc(quest_id, [[
 first
more one
 second
extra two
three by self
 do not grey
]])

game:set_value("c4", 0)
quests_proto:assert_grey(quest_id, {false, false, false, false, false, true})

sol.main.exit()

--dialogs.dat reference
--[[
$@1first
$1more one
$@2second
$2extra two
$3three by self
##$@4do not grey
]]