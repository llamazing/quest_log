--tests whether description lines grey out or not

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

local quest_id = "test.is_grey"
game:set_value("is_grey", 0) --start quest

quests_proto:assert_grey(quest_id, {false, false, false, false, false, false})
quests_proto:assert_desc(quest_id, [[
does not grey
phase 1
more phase 1
always visible
always visible with manual break
end
]])

game:set_value("is_grey", 1)

quests_proto:assert_grey(quest_id, {false, true, true, false, false, false, false})
quests_proto:assert_desc(quest_id, [[
does not grey
phase 1
more phase 1

always visible
always visible with manual break
end
]])

game:set_value("is_grey", 2)

quests_proto:assert_grey(quest_id, {false, true, true, true, true, true, false, false, false, false, false})
quests_proto:assert_desc(quest_id, [[
does not grey
phase 1
more phase 1

mystery phase 2
always visible

temporary phase 3
extra phase 3
always visible with manual break
end
]])

game:set_value("is_grey", 3)

quests_proto:assert_grey(quest_id, {false, true, true, true, true, true, true, false, false, false, false})
quests_proto:assert_desc(quest_id, [[
does not grey
phase 1
more phase 1

mystery phase 2
always visible

always visible with manual break
early phase 4
phase 4
end
]])

game:set_value("is_grey", 4)

quests_proto:assert_grey(quest_id, {true, true, true, true, true, true, true, true, true, true, true})
quests_proto:assert_desc(quest_id, [[
does not grey
phase 1
more phase 1

mystery phase 2
always visible

always visible with manual break
early phase 4
phase 4
end
]])

sol.main.exit()

--dialogs.dat reference
--[[
##does not grey
@phase 1
more phase 1

@?mystery phase 2
#always visible

@!temporary phase 3
!extra phase 3
;##always visible with manual break
early phase 4
@phase 4
##end
]]