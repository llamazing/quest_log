--maps/tests/quest_info/is_grey/test_grey
main{
	dialog_id = "test.is_grey",
	calc_phase = "is_grey",
}

--maps/tests/quest_info/is_grey/test_checkmarks
main{
	dialog_id = "test.grey_checkmarks",
	calc_phase = "checkmarks_grey",
	checkmarks = {
		"c1",
		"c2",
		"c3",
		"c4",
		callback = function(args)
			return {
				not not args[1],
				not not args[2],
				not not args[3],
				not not args[4],
			}
		end
	},
}