--maps/tests/quest_info/is_grey/test_grey
dialog{
	id = "test.is_grey",
	title = "Is Grey Test",
	text = [[
##does not grey
@phase 1
more phase 1

@?mystery phase 2
#always visible

@!temporary phase 3
!extra phase 3
;##always visible with manual break
early phase 4
@phase 4
##end
]]}

--maps/tests/quest_info/is_grey/test_checkmarks
dialog{
	id = "test.grey_checkmarks",
	title = "Is Grey Checkmarks Test",
	text = [[
$@1first
$1more one
$@2second
$2extra two
$3three by self
##$@4do not grey
]]}
