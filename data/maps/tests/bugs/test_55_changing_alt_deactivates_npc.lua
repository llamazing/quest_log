--tests the active NPC when changing the active alternate quest (see issue #55)

local map = ...
local game = map:get_game()

local quests_proto = require"tests/quests_proto"
quests_proto:load_data(map)

game:set_value("bug_55_phase", 0) --start part 1 quest

quests_proto:assert_active_npc("Toby", true)
quests_proto:assert_active_npc("Giles", false)

game:set_value("is_alt", true)
game.objectives:set_alternate("bug_55", "test.bug_55_part2") --make part 2 active

quests_proto:assert_active_npc("Toby", false)
quests_proto:assert_active_npc("Giles", false)

game:set_value("bug_55_phase", 1) --complete phase 1

quests_proto:assert_active_npc("Toby", false)
quests_proto:assert_active_npc("Giles", true)

game.objectives:set_alternate("bug_55", "test.bug_55_part1") --make part 1 active again

quests_proto:assert_active_npc("Toby", false)
quests_proto:assert_active_npc("Giles", false)

game:set_value("bug_55_phase", 2) --complete quest

quests_proto:assert_active_npc("Toby", false)
quests_proto:assert_active_npc("Giles", false)

sol.main.exit()
