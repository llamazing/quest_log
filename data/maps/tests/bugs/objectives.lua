--maps/tests/bugs/test_55_changing_alt_deactivates_npc
main{
	dialog_id = "test.bug_55_part1",
	alternate_key = "bug_55",
	calc_phase = "bug_55_phase",
	npc = {"Toby"},
}

--maps/tests/bugs/test_55_changing_alt_deactivates_npc
main{
	dialog_id = "test.bug_55_part2",
	alternate_key = "bug_55",
	calc_phase = {
		"bug_55_phase",
		"is_alt",
		callback = function(args)
			local phase = args[1]
			local is_alt = args[2] or false
			
			--can only start quest if is_alt is true
			if phase==0 then
				return is_alt and 0
			else return phase end
		end
	},
	npc = {[2]="Giles"},
}
