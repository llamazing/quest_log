--maps/tests/bugs/test_55_changing_alt_deactivates_npc
dialog{
	id = "test.bug_55_part1",
	title = "Changing Alt Deactivates NPC - Part 1",
	text = [[
# Primary quest
@ Phase 1 task
@ Phase 2 task
]]}

--maps/tests/bugs/test_55_changing_alt_deactivates_npc
dialog{
	id = "test.bug_55_part2",
	title = "Changing Alt Deactivates NPC - Part 2",
	text = [[
# Alternate quest
@ Phase 1 task
@ Phase 2 task
]]}
