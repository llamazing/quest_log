-- Lua script of map tower_2.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

function floor_switch:on_activated()
	game:set_value("tower_switch", true)
	switch_bridge:set_enabled(true)
	floor_switch:set_enabled(false)
	switch_floor:set_enabled(false)
end

map:register_event("on_started", function(self)
	if game:get_value"tower_switch" then
		floor_switch:set_enabled(false)
		switch_floor:set_enabled(false)
	else switch_bridge:set_enabled(false) end
end)

function wizard_tower:on_interaction()
	if not game:get_value"tower_done" then
		game:start_dialog("tower_done", function()
			game:set_value("tower_done", true)
		end)
	else game:start_dialog"tower_done_2" end
end
