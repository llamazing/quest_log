-- Lua script of map tower_1.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

function sensor_right:on_activated()
	bridge_right:set_enabled(false)
	sol.audio.play_sound"falling_rock"
	game:set_value("tower1_bridge", "left")
end

function sensor_left:on_activated()
	bridge_left:set_enabled(false)
	sol.audio.play_sound"falling_rock"
	game:set_value("tower1_bridge", "right")
end

map:register_event("on_started", function(self)
	local bridge = game:get_value"tower1_bridge"
	if bridge then
		if bridge=="left" then
			bridge_right:set_enabled(false)
		else bridge_left:set_enabled(false) end
		
		sensor_left:set_enabled(false)
		sensor_right:set_enabled(false)
	end
	
	if game:get_value"tower_intro" then
		wizard_tower:set_enabled(false)
	end
end)

local wizard_timer
function wizard_tower:on_interaction()
	if not game:get_value"tower_intro" then
		game:start_dialog("tower_intro", function()
			game:set_value("tower_intro", true)
			
			if wizard_timer then wizard_timer:stop() end
			wizard_timer = sol.timer.start(map, 1600, function()
				self:set_enabled(false)
				sol.audio.play_sound"bomb"
			end)
		end)
	else game:start_dialog"tower_intro_2" end
end