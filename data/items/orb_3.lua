local item = ...
local game = item:get_game()

-- Event called when the game is initialized.
function item:on_started()
  self:set_savegame_variable"item_orb_3"
  self:set_assignable(false)
  self:set_can_disappear(false)
  self:set_brandish_when_picked(true)
end

function item:on_obtained()
  local save_var = self:get_savegame_variable()
  if save_var then game.objectives:refresh(save_var) end
  
  local map = game:get_map()
  if map.on_orb_obtained then map:on_orb_obtained(self) end
end
