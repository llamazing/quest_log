local item = ...
local game = item:get_game()

-- Event called when the game is initialized.
function item:on_started()
  self:set_amount_savegame_variable"item_count_topaz"
  self:set_savegame_variable"item_topaz"
  self:set_assignable(false)
  self:set_can_disappear(false)
  self:set_brandish_when_picked(false)
  self:set_max_amount(99)
end

function item:on_obtaining(variant, savegame_variable)
  self:add_amount(1)
end

function item:on_amount_changed(amount)
  local save_var = self:get_amount_savegame_variable()
  if save_var then game.objectives:refresh(save_var) end
end

function item:on_obtained()
  local save_var = self:get_savegame_variable()
  if save_var then game.objectives:refresh(save_var) end
end
