--[[ armor.lua
	version 1.0
	1 Jan 2019
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/
	
	This custom entity script manages a suit of armor that reacts to the player hitting it
	with the sword. In addition to displaying an animation when hit, it also shows a count
	of the number of times hit above its head. There is a maximum time between consecutive
	hits, after which the hit counter resets to zero. There is also an upper limit for the
	number of consecutive hits, after which the counter is no longer updated (default: max
	of 5 consecutive hits).
	
	If the custom entity defines a custom_entity:on_last_hit(count) event, then it will be
	called after the last hit or when the max is reached. The number of times hit is given
	by the count parameter (number, positive integer). 
]]

local entity = ...
local map = entity:get_map()

--local variables
local state --possible values: "stopped", "hit"
local hit_count --number of consecutive hits, up to the value of MAX_HITS
local hit_timer --timer for period of invulnerability after a hit
local consecutive_timer --timer for period between consecutive hits
local hit_entity

--constants
local MAX_HITS = 5 --the maximum number of consecutive hits that will be registered
local HIT_DURATION = 320 --invulnerable for this duration (ms) after getting hit
local CONSECUTIVE_DURATION = 1200 --max time (ms) between consecutive hits


--## Implementation ##--

--// tests for a sprite collision with the player's sword, changing to "hit" state
local function collision_test(entity1, entity2)
	local entity_name = entity2:get_name()
	local animation = entity2:get_sprite():get_animation()
	
	if state=="stopped" and entity_name=="hero" and animation=="sword" then
		entity:hit()
	end
end

--// callback function for the consecutive_timer, resets hit count when expired
local function consecutive_cb()
	if type(entity.on_last_hit)=="function" then entity:on_last_hit(hit_count) end
	
	hit_count = 0
	consecutive_timer = nil
	if hit_entity then hit_entity:remove() end
end

local function create_hit_sprite(count)
	if hit_entity then hit_entity:remove() end
	
	local x,y = entity:get_position()
	
	hit_entity = map:create_custom_entity{
		direction = 0,
		layer = 1,
		x = x,
		y = y - 22,
		width = 24,
		height = 16,
		sprite = "entities/hit_"..count,
	}
	
	local movement = sol.movement.create"straight"
	movement:set_angle(math.pi/2) --up
	movement:set_max_distance(4)
	movement:set_speed(8) --500ms for movement
	movement:start(hit_entity, function()
		hit_entity:get_sprite():fade_out() --620ms for fade out
	end)
end

--// changes entity to the "hit" state and changes the sprite animation then returns to "stopped" state again
function entity:hit()
	if state ~= "hit" then
		state = "hit"
		sol.audio.play_sound"shield"
		
		local sprite = entity:get_sprite()
		sprite:set_animation("hit", "stopped")
		hit_timer = sol.timer.start(self, HIT_DURATION, function()
			state = "stopped"
			hit_timer = nil
		end)
		
		if consecutive_timer then
			if hit_count < MAX_HITS then
				consecutive_timer:stop()
				
				hit_count = hit_count + 1
				consecutive_timer = sol.timer.start(self, CONSECUTIVE_DURATION, consecutive_cb)
				create_hit_sprite(hit_count)
			end
		else
			hit_count = 1
			consecutive_timer = sol.timer.start(self, CONSECUTIVE_DURATION, consecutive_cb)
			create_hit_sprite(hit_count)
		end
	else return false end
	
	return true --successfully changed state
end


--## Events ##--

function entity:on_created()
	state = "stopped"
	hit_count = 0
	
	self:set_traversable_by(false)
	entity:set_drawn_in_y_order(true)
	entity:add_collision_test("sprite", collision_test) --test for hit by player's sword
end

--[[ Copyright 2018-2019 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
