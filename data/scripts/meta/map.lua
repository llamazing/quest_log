--Initialize map behavior specific to this quest.

require"scripts/multi_events"

local map_meta = sol.main.get_metatable"map"

--constants
local ICON_Y = 24 --y offset in pixels for height of icon above origin

--Disables and enables quest icons for NPCs on the current map. Called automatically when quests update
local function update_npcs(self)
	local game = self:get_game()
	if not self.icons then self.icons = {} end
	
	--deactivate any existing icons that are no longer active
	for npc_name,sprite in pairs(self.icons) do
		if not game.objectives:is_npc_active(npc_name) then
			local npc = self:get_entity(npc_name)
			if npc then
				npc:remove_sprite(sprite)
				self.icons[npc_name] = nil
			end
		end
	end
	
	local is_new = false --true if sprite added to npc, tentative
	for npc_name in game.objectives:active_npcs() do
		if not self.icons[npc_name] then
			local npc = self:get_entity(npc_name)
			if npc then
				local sprite = npc:create_sprite("entities/exclamation", "quest_icon")
				local icon_offset = npc:get_property"icon_offset_y" or 0 --additional offset for tall NPCs
				sprite:set_xy(0, -ICON_Y-icon_offset)
				self.icons[npc_name] = sprite
				is_new = true
			end
		end
	end
	
	return is_new
end

map_meta.update_icons = update_npcs
map_meta:register_event("on_started", function(map)
	--update NPC icons after 1.5 second delay
	sol.timer.start(map, 1500, function()
		if update_npcs(map) then sol.audio.play_sound"frost1" end
	end)
end)

return true
