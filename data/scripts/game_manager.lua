--Script that creates a game ready to be played.

--Usage:
--local game_manager = require("scripts/game_manager")
--local game = game_manager:create("savegame_file_name")
--game:start()

require"scripts/multi_events"
local initial_game = require"scripts/initial_game"
local pause_menu = require"scripts/menus/pause_menu"
local objectives_manager = require"scripts/objectives_manager"

local game_manager = {}

--name of sound to play for different new task status keywords
local QUEST_SOUNDS = {
	main_all_completed = "quest_complete", --TODO need sound
	side_all_completed = "quest_complete", --TODO need sound
	main_completed = "quest_complete",
	side_completed = "quest_complete",
	main_started = "quest_started",
	side_started = "quest_started",
	main_advanced = "quest_advance",
	side_advanced = "quest_advance",
	new_checkmark = "quest_subtask",
	progressed_quest_item = "quest_subtask",
	main_advanced_again = false, --don't play sound
	side_advanced_again = false, --don't play sound
	alternate_swap = false, --don't play sound
	forced_update = false, --don't play sound
}

--Creates a game ready to be played.
function game_manager:create(file)
	local exists = sol.game.exists(file)
	local uptime = sol.main.get_elapsed_time()
	
	--purge any previous savegame data when starting Solarus fresh
	local is_loading_save
	if exists then
		if uptime < 5000 then
			sol.game.delete(file)
			exists = nil
		else is_loading_save = true end --keep track that the game was restarted so can start off with wavy effect fade in
	end
	
	--create the game but do not start it
	local game = sol.game.load(file)
	game.is_loading_save = is_loading_save
	
	if not exists then --This is a new savegame file.
		initial_game:initialize_new_savegame(game)
	else game:set_value("loaded_save", true) end
	
	return game
end

local game_meta = sol.main.get_metatable"game"
game_meta:register_event("on_started", function(game)
	objectives_manager.create(game)
	
	--// Event called whenever a quest is updated (or added to log or completed)
	function game.objectives:on_quest_updated(status, dialog_id)
		game:get_map():update_icons()
		if self:is_new_task() then game:set_hud_icon("pause", "quest_alert") end
		
		--play sound depending on the status
		local sound_name = QUEST_SOUNDS[status]
		if sound_name then sol.audio.play_sound(sound_name) end
		
		--display dialog for starting a new quest
		if (status=="main_started" or status=="side_started") and not game:is_dialog_enabled() then
			local dialog_title = sol.language.get_dialog(dialog_id).title
			game:start_dialog("system.new_quest", dialog_title, function()
				local map = game:get_map()
				if map and map.resume_dialog then map.resume_dialog() end
			end)
		end
	end
	
	--// Event called when ALL quests have been completed
	function game.objectives:on_all_quests_done()
		if game:get_value"loaded_save" then --player did time warp at least once (otherwise quest is not done yet)
			--play fanfare music
			sol.audio.play_music("eduardo/dungeon_finished", function()
				sol.audio.play_music"eduardo/cave"
			end)
			
			--show dialog asking player if they want to start over
			game:start_dialog("system.all_complete", function()
				game:restart_prompt()
			end)
		end
	end
	
	--// start question dialog asking if the player wants to start a new game, and then restart if yes is chosen
	function game:restart_prompt()
		self:start_dialog("system.new_game", function(answer)
			if answer==1 then --new game
				if sol.game.exists(file) then sol.game.delete(file) end
				sol.main.reset()
			end
		end)
	end
	
	--// Clear flashing quests updated icon on HUD when player opens quest log
	function game.objectives:on_tasks_cleared()
		game:set_hud_icon("pause", "normal")
	end
	
	game:register_event("on_started", function()
		sol.audio.set_music_volume(15)
	end)
	
	game:register_event("on_paused", function()
		if not sol.menu.is_started(pause_menu) then sol.menu.start(game, pause_menu) end
	end)
	
	game:register_event("on_key_pressed", function(self, key)
		local submenu_index = pause_menu.quick_keys[key]
		if submenu_index then
			pause_menu:toggle_submenu(submenu_index)
			return true
		end
	end)
	
	function game.set_custom_command_effect() end --do nothing
end)

return game_manager
