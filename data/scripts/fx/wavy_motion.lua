--[[ wavy_motion.lua
	version 1.0
	2 Feb 2019
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script manages the "wavy" shader by varying its settings over time to animate the
	appearance.
	
	usage:
	local wavy_motion = require"scripts/fx/wavy_motion"
	wavy_motion:start_effect(surface, game, "FADE_OUT", callback)
]]

local shader_controller = {}

--TODO move these to separate util script
--converts a value from 0 to 1 to a different value 0 to 1, i.e. making a linear sweep non-linear
SWEEPS = {
	cos_pos = function(percent) return (1 - math.cos(percent*math.pi))/2 end, --slow, fast, slow: increasing
	cos_neg = function(percent) return (1 + math.cos(percent*math.pi))/2 end, --slow, fast, slow: decreasing
	half_cos_pos = function(percent) return 1 - math.cos(percent*math.pi/2) end, --slow, fast: increasing
	half_cos_neg = function(percent) return math.cos(percent*math.pi/2) end, --fast, slow: decreasing
	sin_pos = function(percent) return math.sin(percent*math.pi/2) end, --fast, slow: increasing
	sin_neg = function(percent) return 1 - math.sin(percent*math.pi/2) end, --slow, fast: decreasing
	split_sin_pos = function(percent) --fast, slow, fast: increasing
		if percent <= 0.5 then
			return math.sin(percent*math.pi)/2
		else return 1 - math.sin(percent*math.pi)/2 end
	end,
	split_sin_neg = function(percent) --fast, slow, fast: decreasing
		if percent <= 0.5 then
			return 1 - math.sin(percent*math.pi)/2
		else return math.sin(percent*math.pi)/2 end
	end,
	linear_pos = function(percent) return percent end, --i.e. no change
	linear_neg = function(percent) return 1 - percent end, --decreasing linear
}

--all possible behaviors for a given style in 'start_effect'
STYLES = {
	FADE_OUT = function(shader, delta_time)
		local DURATION = 5000 --in ms
		local MIN_MAGNITUDE = 0
		local MAX_MAGNITUDE = 0.1
		
		local percent = delta_time/DURATION
		if percent > 1 then return false end
		
		local percent2 = SWEEPS.sin_pos(percent)
		local magnitude = MIN_MAGNITUDE + percent2*(MAX_MAGNITUDE - MIN_MAGNITUDE)
		
		shader:set_uniform("amplitude", magnitude)
		shader:set_uniform("phase", 6*math.pi*delta_time/DURATION)
		
		return true
	end,
	FADE_IN = function (shader, delta_time)
		local DURATION = 1500 --in ms
		local MIN_MAGNITUDE = 0
		local MAX_MAGNITUDE = 0.1
		
		local percent = delta_time/DURATION
		if percent > 1 then return false end
		
		local percent2 = SWEEPS.sin_neg(percent)
		local magnitude = MIN_MAGNITUDE + percent2*(MAX_MAGNITUDE - MIN_MAGNITUDE)
		
		shader:set_uniform("amplitude", magnitude)
		shader:set_uniform("phase", 6*math.pi*delta_time/5000)
		
		return true
	end,
}

--// Applies a wavy effect to a drawable object
	--surface (sol.drawable) - the drawable object to apply the fade-out effect to
	--context (various) - the context to use for the timer created -- see sol.timer.start() for more info
	--style (string) - The type of transition to use for the effect. Possible values:
		--"FADE_OUT" - wavyness gradually increases for up to 5 seconds to reach peak
		--"FADE_IN" - opposite of FADE_OUT but happens faster, in 1.5 seconds
	--callback (function, optional) - callback function for when the animation is finished
	--returns the sol.timer used for the animation. To abort the animation early, use timer:stop()
function shader_controller:start_effect(surface, context, style, callback)
	assert(surface and surface.set_shader, "Bad argument #2 to 'start_effect' (does not have set_shader method)")
	assert(type(style)=="string", "Bad argument #4 to 'start_effect' (string expected)")
	local action = STYLES[style]
	assert(action, "Bad argument #4 to 'start_effect', invalid style: "..style)
	
	local start_time = sol.main.get_elapsed_time()
	local shader = sol.shader.create"wavy"
	shader:set_uniform("amplitude", 0)
	surface:set_shader(shader)
	
	--repeatedly update values for shader every 10ms
	return sol.timer.start(context, 10, function()
		local delta_time = sol.main.get_elapsed_time() - start_time
		if not action(shader, delta_time) then --end of animation reached
			surface:set_shader(nil)
			if callback then callback() end
			return false --stop timer
		end
		
		return true --repeat timer
	end)
end

return shader_controller

--[[ Copyright 2019 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
