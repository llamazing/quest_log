--This script initializes game values for a new savegame file.
--You should modify the initialize_new_savegame() function below to set values like
--the initial life and equipment as well as the starting location.
--
--Usage:
--local initial_game = require("scripts/initial_game")
--initial_game:initialize_new_savegame(game)

local initial_game = {}

--Sets initial values to a new savegame file
function initial_game:initialize_new_savegame(game)
	game:set_starting_location("entrance", nil)  --Starting location.
	
	game:set_value("player_name", sol.language.get_string"player_name")
	game:set_max_life(12)
	game:set_life(game:get_max_life())
	game:set_ability("lift", 1)
	game:set_ability("sword", 1)
	
	--game:set_value("quest_test1", 0) --DEBUG
	--game:set_value("quest_test2", 0) --DEBUG
	--game:set_value("quest_test3", 0) --DEBUG
	--game:set_value("quest_test4", 0) --DEBUG
end

return initial_game
