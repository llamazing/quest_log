--Sets up all non built-in gameplay features specific to this quest

--Usage: require("scripts/features")

--Features can be enabled or disabled independently by commenting or uncommenting lines below
require"scripts/hud/hud"
require"scripts/meta/hero"
require"scripts/mouse_control"
require"scripts/menus/dialog_box.lua"

--metas
require"scripts/meta/map.lua"

return true
