/*	wavy.frag.glsl
	version 1.0
	29 Jan 2019
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This GLSL fragment shader draws time-warp style wavy lines that are modulated by using
	the following uniform variables:
	* amplitude (float) - the magnitude of the distortion
	* phase (float) - vertical offset of the distortion
*/

#define PI 3.14159265359

#if __VERSION__ >= 130
#define COMPAT_VARYING in
#define COMPAT_TEXTURE texture
out vec4 FragColor;
#else
#define COMPAT_VARYING varying
#define FragColor gl_FragColor
#define COMPAT_TEXTURE texture2D
#endif

#ifdef GL_ES
precision mediump float;
#define COMPAT_PRECISION mediump
#else
#define COMPAT_PRECISION
#endif

uniform sampler2D sol_texture;
COMPAT_VARYING vec2 sol_vtex_coord;
COMPAT_VARYING vec4 sol_vcolor;

uniform float amplitude = 0.1;
uniform float phase = 0;

void main() {
	vec4 junk = sol_vcolor; //begone warning message
	float x = amplitude * sin(8*PI*sol_vtex_coord.y + phase);
	
	vec2 new_coords = vec2(sol_vtex_coord.x+x, sol_vtex_coord.y);
	FragColor = COMPAT_TEXTURE(sol_texture, new_coords);
}

/* Copyright 2019 Llamazing
 * 
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this
 * program.  If not, see <http://www.gnu.org/licenses/>.
 */
