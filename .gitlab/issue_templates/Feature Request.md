## Summary

(Give a short description of what you'd like the newly proposed feature to accomplish)

## Description
### Background - How it is useful

(Describe what is happening in your quest project where the new feature would be useful or otherwise describe the situation in which it would be used).

### Mock-up

(give a hypothetical example of what a dialogs.dat or objectives.dat entry might look like using the proposed feature, or a lua code excerpt of how your script would interact with the quest log scripts)

```lua
Example code
```

(include mock-up screenshots, if applicable)

/label ~"Feature Request" 