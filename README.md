# Quest Log Menu for the Solarus Engine

This script creates a quest log menu for the Solarus engine to keep track of and display progress for main quests and side quests. It is highly customizable, and special dialog syntax combined with usage of custom functions allows for a wide amount of flexibility.

![quest_log_preview](/uploads/c3710ed3b38579c76eb145736c9c938c/quest_log_preview.png)

**Features include:**

* A sidebar that can switch between listing main quests and side quests
* List the number of quests completed out of the total number of quests (can be hidden until some end-game milestone is achieved)
* A description pane for the selected quest displays the quest title and lists the objectives (which can be appended to as the quest progresses)
   * Objectives and subtasks for the quest are checked off as they are completed
* Location text can be specified for each quest that can change during each phase of the quest
* Display an icon above the heads of NPCs relevant to a quest during certain phases
* Display status of quest items (e.g. a trade quest) in the description text, including displaying item icons
* Full localization support
* Customization of menu elements such as sprites and fonts used, size and position
* Custom events make it easy to display an icon on the HUD or play sounds whenever a quest is updated

**Advanced Features**

* Display any item icon in quest description text
* Dynamic substitution of quest description text using custom functions
* Complex quest progression based on the state of savegame variables using custom functions
   * Certain run-time variables may also be used: current map world, map id, map floor
* Non-linear quest progression through the use of dynamic checkmarks and alternate quests
* Manipulate elements of the quest log menu from your own scripts

## Getting Started

This project contains an example quest that can be run using Solarus. It is a small map containing a few rooms, each with their own sidequests showcasing a variety of capabilities of the quest log script. Follow these [installation](https://gitlab.com/llamazing/quest_log/wikis/installation) instructions.

The project [wiki](https://gitlab.com/llamazing/quest_log/wikis/home) contains a wealth of information detailing how to use the scripts along with example code.

## Additional Support

To report a bug or request additional features, please create an [issue](https://gitlab.com/llamazing/quest_log/issues).

If you need help using a particular feature or would like assistance in creating a quest for the quest log for your own project, feel free to open an issue for that as well. Please be as detailed as possible in describing what you'd like to accomplish, and include example code from your own project where applicable.

